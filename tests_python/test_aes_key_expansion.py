"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://csrc.nist.gov/pubs/fips/197/final
Ref: https://csrc.nist.gov/projects/cryptographic-algorithm-validation-program/block-ciphers#AES
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_256_N_K,
    AES_192_N_K,
    AES_128_N_K,
    AES_256_N_B,
    AES_192_N_B,
    AES_128_N_B,
    AES_256_N_R,
    AES_192_N_R,
    AES_128_N_R,
    AES_256_RCON_BYTES,
    AES_192_RCON_BYTES,
    AES_128_RCON_BYTES,
)

from explore_aes_python.aes_key_expansion import (
    rotword_bytes,
    subword_bytes,
    key_expansion_bytes,
    key_expansion_eic_bytes,
)


# Taken from Wikipedia; https://en.wikipedia.org/wiki/AES_key_schedule
# Pre-fixed with 0x00 in position i=0
RCON_CHECK = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36]


class TestAesKeyExpansionFunctions:
    """A unit test class."""

    def test_rot_word_bytes(self):
        """A unit test function."""
        _in_check = bytes.fromhex("00010203")
        _out_check = bytes.fromhex("01020300")

        _out_ut = rotword_bytes(_in_check)

        assert _out_ut == _out_check

        _in_check = [
            "09cf4f3c",
            "2a6c7605",
            "7359f67f",
            "6d7a883b",
            "db0bad00",
            "11f915bc",
            "ca0093fd",
            "4ea6dc4f",
            "7f8d292f",
            "575c006e",
        ]
        _out_check = [
            "cf4f3c09",
            "6c76052a",
            "59f67f73",
            "7a883b6d",
            "0bad00db",
            "f915bc11",
            "0093fdca",
            "a6dc4f4e",
            "8d292f7f",
            "5c006e57",
        ]

        for _i, _ic in enumerate(_in_check):
            _in_word = bytes.fromhex(_in_check[_i])
            _out_word = bytes.fromhex(_out_check[_i])

            _out_ut = rotword_bytes(_in_word)

            assert _out_ut == _out_word

    def test_sub_word_bytes(self):
        """A unit test function."""
        _in_check = [
            "cf4f3c09",
            "6c76052a",
            "59f67f73",
            "7a883b6d",
            "0bad00db",
            "f915bc11",
            "0093fdca",
            "a6dc4f4e",
            "8d292f7f",
            "5c006e57",
        ]
        _out_check = [
            "8a84eb01",
            "50386be5",
            "cb42d28f",
            "dac4e23c",
            "2b9563b9",
            "99596582",
            "63dc5474",
            "2486842f",
            "5da515d2",
            "4a639f5b",
        ]

        for _i, _ic in enumerate(_in_check):
            _in_word = bytes.fromhex(_in_check[_i])
            _out_word = bytes.fromhex(_out_check[_i])

            _out_ut = subword_bytes(_in_word)

            assert _out_ut == _out_word


class TestAesKeyExpansionBytes:
    """A unit test class."""

    # fmt: off
    # OUT_CHECK_128 =[temp, After RotWord(), After SubWord(), Rcon[i/Nk],
    #                 After XOR with Rcon, w[i–Nk], w[i]=temp XOR w[i-Nk]
    _OUT_CHECK_128 = [
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '2b7e1516'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '28aed2a6'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', 'abf71588'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '09cf4f3c'],
        ['09cf4f3c', 'cf4f3c09', '8a84eb01', '01000000', '8b84eb01', '2b7e1516', 'a0fafe17'],
        ['a0fafe17', '00000000', '00000000', '00000000', '00000000', '28aed2a6', '88542cb1'],
        ['88542cb1', '00000000', '00000000', '00000000', '00000000', 'abf71588', '23a33939'],
        ['23a33939', '00000000', '00000000', '00000000', '00000000', '09cf4f3c', '2a6c7605'],
        ['2a6c7605', '6c76052a', '50386be5', '02000000', '52386be5', 'a0fafe17', 'f2c295f2'],
        ['f2c295f2', '00000000', '00000000', '00000000', '00000000', '88542cb1', '7a96b943'],
        ['7a96b943', '00000000', '00000000', '00000000', '00000000', '23a33939', '5935807a'],
        ['5935807a', '00000000', '00000000', '00000000', '00000000', '2a6c7605', '7359f67f'],
        ['7359f67f', '59f67f73', 'cb42d28f', '04000000', 'cf42d28f', 'f2c295f2', '3d80477d'],
        ['3d80477d', '00000000', '00000000', '00000000', '00000000', '7a96b943', '4716fe3e'],
        ['4716fe3e', '00000000', '00000000', '00000000', '00000000', '5935807a', '1e237e44'],
        ['1e237e44', '00000000', '00000000', '00000000', '00000000', '7359f67f', '6d7a883b'],
        ['6d7a883b', '7a883b6d', 'dac4e23c', '08000000', 'd2c4e23c', '3d80477d', 'ef44a541'],
        ['ef44a541', '00000000', '00000000', '00000000', '00000000', '4716fe3e', 'a8525b7f'],
        ['a8525b7f', '00000000', '00000000', '00000000', '00000000', '1e237e44', 'b671253b'],
        ['b671253b', '00000000', '00000000', '00000000', '00000000', '6d7a883b', 'db0bad00'],
        ['db0bad00', '0bad00db', '2b9563b9', '10000000', '3b9563b9', 'ef44a541', 'd4d1c6f8'],
        ['d4d1c6f8', '00000000', '00000000', '00000000', '00000000', 'a8525b7f', '7c839d87'],
        ['7c839d87', '00000000', '00000000', '00000000', '00000000', 'b671253b', 'caf2b8bc'],
        ['caf2b8bc', '00000000', '00000000', '00000000', '00000000', 'db0bad00', '11f915bc'],
        ['11f915bc', 'f915bc11', '99596582', '20000000', 'b9596582', 'd4d1c6f8', '6d88a37a'],
        ['6d88a37a', '00000000', '00000000', '00000000', '00000000', '7c839d87', '110b3efd'],
        ['110b3efd', '00000000', '00000000', '00000000', '00000000', 'caf2b8bc', 'dbf98641'],
        ['dbf98641', '00000000', '00000000', '00000000', '00000000', '11f915bc', 'ca0093fd'],
        ['ca0093fd', '0093fdca', '63dc5474', '40000000', '23dc5474', '6d88a37a', '4e54f70e'],
        ['4e54f70e', '00000000', '00000000', '00000000', '00000000', '110b3efd', '5f5fc9f3'],
        ['5f5fc9f3', '00000000', '00000000', '00000000', '00000000', 'dbf98641', '84a64fb2'],
        ['84a64fb2', '00000000', '00000000', '00000000', '00000000', 'ca0093fd', '4ea6dc4f'],
        ['4ea6dc4f', 'a6dc4f4e', '2486842f', '80000000', 'a486842f', '4e54f70e', 'ead27321'],
        ['ead27321', '00000000', '00000000', '00000000', '00000000', '5f5fc9f3', 'b58dbad2'],
        ['b58dbad2', '00000000', '00000000', '00000000', '00000000', '84a64fb2', '312bf560'],
        ['312bf560', '00000000', '00000000', '00000000', '00000000', '4ea6dc4f', '7f8d292f'],
        ['7f8d292f', '8d292f7f', '5da515d2', '1b000000', '46a515d2', 'ead27321', 'ac7766f3'],
        ['ac7766f3', '00000000', '00000000', '00000000', '00000000', 'b58dbad2', '19fadc21'],
        ['19fadc21', '00000000', '00000000', '00000000', '00000000', '312bf560', '28d12941'],
        ['28d12941', '00000000', '00000000', '00000000', '00000000', '7f8d292f', '575c006e'],
        ['575c006e', '5c006e57', '4a639f5b', '36000000', '7c639f5b', 'ac7766f3', 'd014f9a8'],
        ['d014f9a8', '00000000', '00000000', '00000000', '00000000', '19fadc21', 'c9ee2589'],
        ['c9ee2589', '00000000', '00000000', '00000000', '00000000', '28d12941', 'e13f0cc8'],
        ['e13f0cc8', '00000000', '00000000', '00000000', '00000000', '575c006e', 'b6630ca6'],
    ]


    # OUT_CHECK_192 =[temp, After RotWord(), After SubWord(), Rcon[i/Nk],
    #                 After XOR with Rcon, w[i–Nk], w[i]=temp XOR w[i-Nk]
    _OUT_CHECK_192 = [
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '8e73b0f7'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', 'da0e6452'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', 'c810f32b'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '809079e5'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '62f8ead2'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '522c6b7b'],
        ['522c6b7b', '2c6b7b52', '717f2100', '01000000', '707f2100', '8e73b0f7', 'fe0c91f7'],
        ['fe0c91f7', '00000000', '00000000', '00000000', '00000000', 'da0e6452', '2402f5a5'],
        ['2402f5a5', '00000000', '00000000', '00000000', '00000000', 'c810f32b', 'ec12068e'],
        ['ec12068e', '00000000', '00000000', '00000000', '00000000', '809079e5', '6c827f6b'],
        ['6c827f6b', '00000000', '00000000', '00000000', '00000000', '62f8ead2', '0e7a95b9'],
        ['0e7a95b9', '00000000', '00000000', '00000000', '00000000', '522c6b7b', '5c56fec2'],
        ['5c56fec2', '56fec25c', 'b1bb254a', '02000000', 'b3bb254a', 'fe0c91f7', '4db7b4bd'],
        ['4db7b4bd', '00000000', '00000000', '00000000', '00000000', '2402f5a5', '69b54118'],
        ['69b54118', '00000000', '00000000', '00000000', '00000000', 'ec12068e', '85a74796'],
        ['85a74796', '00000000', '00000000', '00000000', '00000000', '6c827f6b', 'e92538fd'],
        ['e92538fd', '00000000', '00000000', '00000000', '00000000', '0e7a95b9', 'e75fad44'],
        ['e75fad44', '00000000', '00000000', '00000000', '00000000', '5c56fec2', 'bb095386'],
        ['bb095386', '095386bb', '01ed44ea', '04000000', '05ed44ea', '4db7b4bd', '485af057'],
        ['485af057', '00000000', '00000000', '00000000', '00000000', '69b54118', '21efb14f'],
        ['21efb14f', '00000000', '00000000', '00000000', '00000000', '85a74796', 'a448f6d9'],
        ['a448f6d9', '00000000', '00000000', '00000000', '00000000', 'e92538fd', '4d6dce24'],
        ['4d6dce24', '00000000', '00000000', '00000000', '00000000', 'e75fad44', 'aa326360'],
        ['aa326360', '00000000', '00000000', '00000000', '00000000', 'bb095386', '113b30e6'],
        ['113b30e6', '3b30e611', 'e2048e82', '08000000', 'ea048e82', '485af057', 'a25e7ed5'],
        ['a25e7ed5', '00000000', '00000000', '00000000', '00000000', '21efb14f', '83b1cf9a'],
        ['83b1cf9a', '00000000', '00000000', '00000000', '00000000', 'a448f6d9', '27f93943'],
        ['27f93943', '00000000', '00000000', '00000000', '00000000', '4d6dce24', '6a94f767'],
        ['6a94f767', '00000000', '00000000', '00000000', '00000000', 'aa326360', 'c0a69407'],
        ['c0a69407', '00000000', '00000000', '00000000', '00000000', '113b30e6', 'd19da4e1'],
        ['d19da4e1', '9da4e1d1', '5e49f83e', '10000000', '4e49f83e', 'a25e7ed5', 'ec1786eb'],
        ['ec1786eb', '00000000', '00000000', '00000000', '00000000', '83b1cf9a', '6fa64971'],
        ['6fa64971', '00000000', '00000000', '00000000', '00000000', '27f93943', '485f7032'],
        ['485f7032', '00000000', '00000000', '00000000', '00000000', '6a94f767', '22cb8755'],
        ['22cb8755', '00000000', '00000000', '00000000', '00000000', 'c0a69407', 'e26d1352'],
        ['e26d1352', '00000000', '00000000', '00000000', '00000000', 'd19da4e1', '33f0b7b3'],
        ['33f0b7b3', 'f0b7b333', '8ca96dc3', '20000000', 'aca96dc3', 'ec1786eb', '40beeb28'],
        ['40beeb28', '00000000', '00000000', '00000000', '00000000', '6fa64971', '2f18a259'],
        ['2f18a259', '00000000', '00000000', '00000000', '00000000', '485f7032', '6747d26b'],
        ['6747d26b', '00000000', '00000000', '00000000', '00000000', '22cb8755', '458c553e'],
        ['458c553e', '00000000', '00000000', '00000000', '00000000', 'e26d1352', 'a7e1466c'],
        ['a7e1466c', '00000000', '00000000', '00000000', '00000000', '33f0b7b3', '9411f1df'],
        ['9411f1df', '11f1df94', '82a19e22', '40000000', 'c2a19e22', '40beeb28', '821f750a'],
        ['821f750a', '00000000', '00000000', '00000000', '00000000', '2f18a259', 'ad07d753'],
        ['ad07d753', '00000000', '00000000', '00000000', '00000000', '6747d26b', 'ca400538'],
        ['ca400538', '00000000', '00000000', '00000000', '00000000', '458c553e', '8fcc5006'],
        ['8fcc5006', '00000000', '00000000', '00000000', '00000000', 'a7e1466c', '282d166a'],
        ['282d166a', '00000000', '00000000', '00000000', '00000000', '9411f1df', 'bc3ce7b5'],
        ['bc3ce7b5', '3ce7b5bc', 'eb94d565', '80000000', '6b94d565', '821f750a', 'e98ba06f'],
        ['e98ba06f', '00000000', '00000000', '00000000', '00000000', 'ad07d753', '448c773c'],
        ['448c773c', '00000000', '00000000', '00000000', '00000000', 'ca400538', '8ecc7204'],
        ['8ecc7204', '00000000', '00000000', '00000000', '00000000', '8fcc5006', '01002202'],
    ]


    # OUT_CHECK_256 =[temp, After RotWord(), After SubWord(), Rcon[i/Nk],
    #                 After XOR with Rcon, w[i–Nk], w[i]=temp XOR w[i-Nk]
    _OUT_CHECK_256 = [
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '603deb10'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '15ca71be'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '2b73aef0'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '857d7781'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '1f352c07'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '3b6108d7'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '2d9810a3'],
        ['00000000', '00000000', '00000000', '00000000', '00000000', '00000000', '0914dff4'],
        ['0914dff4', '14dff409', 'fa9ebf01', '01000000', 'fb9ebf01', '603deb10', '9ba35411'],
        ['9ba35411', '00000000', '00000000', '00000000', '00000000', '15ca71be', '8e6925af'],
        ['8e6925af', '00000000', '00000000', '00000000', '00000000', '2b73aef0', 'a51a8b5f'],
        ['a51a8b5f', '00000000', '00000000', '00000000', '00000000', '857d7781', '2067fcde'],
        ['2067fcde', '00000000', 'b785b01d', '00000000', '00000000', '1f352c07', 'a8b09c1a'],
        ['a8b09c1a', '00000000', '00000000', '00000000', '00000000', '3b6108d7', '93d194cd'],
        ['93d194cd', '00000000', '00000000', '00000000', '00000000', '2d9810a3', 'be49846e'],
        ['be49846e', '00000000', '00000000', '00000000', '00000000', '0914dff4', 'b75d5b9a'],
        ['b75d5b9a', '5d5b9ab7', '4c39b8a9', '02000000', '4e39b8a9', '9ba35411', 'd59aecb8'],
        ['d59aecb8', '00000000', '00000000', '00000000', '00000000', '8e6925af', '5bf3c917'],
        ['5bf3c917', '00000000', '00000000', '00000000', '00000000', 'a51a8b5f', 'fee94248'],
        ['fee94248', '00000000', '00000000', '00000000', '00000000', '2067fcde', 'de8ebe96'],
        ['de8ebe96', '00000000', '1d19ae90', '00000000', '00000000', 'a8b09c1a', 'b5a9328a'],
        ['b5a9328a', '00000000', '00000000', '00000000', '00000000', '93d194cd', '2678a647'],
        ['2678a647', '00000000', '00000000', '00000000', '00000000', 'be49846e', '98312229'],
        ['98312229', '00000000', '00000000', '00000000', '00000000', 'b75d5b9a', '2f6c79b3'],
        ['2f6c79b3', '6c79b32f', '50b66d15', '04000000', '54b66d15', 'd59aecb8', '812c81ad'],
        ['812c81ad', '00000000', '00000000', '00000000', '00000000', '5bf3c917', 'dadf48ba'],
        ['dadf48ba', '00000000', '00000000', '00000000', '00000000', 'fee94248', '24360af2'],
        ['24360af2', '00000000', '00000000', '00000000', '00000000', 'de8ebe96', 'fab8b464'],
        ['fab8b464', '00000000', '2d6c8d43', '00000000', '00000000', 'b5a9328a', '98c5bfc9'],
        ['98c5bfc9', '00000000', '00000000', '00000000', '00000000', '2678a647', 'bebd198e'],
        ['bebd198e', '00000000', '00000000', '00000000', '00000000', '98312229', '268c3ba7'],
        ['268c3ba7', '00000000', '00000000', '00000000', '00000000', '2f6c79b3', '09e04214'],
        ['09e04214', 'e0421409', 'e12cfa01', '08000000', 'e92cfa01', '812c81ad', '68007bac'],
        ['68007bac', '00000000', '00000000', '00000000', '00000000', 'dadf48ba', 'b2df3316'],
        ['b2df3316', '00000000', '00000000', '00000000', '00000000', '24360af2', '96e939e4'],
        ['96e939e4', '00000000', '00000000', '00000000', '00000000', 'fab8b464', '6c518d80'],
        ['6c518d80', '00000000', '50d15dcd', '00000000', '00000000', '98c5bfc9', 'c814e204'],
        ['c814e204', '00000000', '00000000', '00000000', '00000000', 'bebd198e', '76a9fb8a'],
        ['76a9fb8a', '00000000', '00000000', '00000000', '00000000', '268c3ba7', '5025c02d'],
        ['5025c02d', '00000000', '00000000', '00000000', '00000000', '09e04214', '59c58239'],
        ['59c58239', 'c5823959', 'a61312cb', '10000000', 'b61312cb', '68007bac', 'de136967'],
        ['de136967', '00000000', '00000000', '00000000', '00000000', 'b2df3316', '6ccc5a71'],
        ['6ccc5a71', '00000000', '00000000', '00000000', '00000000', '96e939e4', 'fa256395'],
        ['fa256395', '00000000', '00000000', '00000000', '00000000', '6c518d80', '9674ee15'],
        ['9674ee15', '00000000', '90922859', '00000000', '00000000', 'c814e204', '5886ca5d'],
        ['5886ca5d', '00000000', '00000000', '00000000', '00000000', '76a9fb8a', '2e2f31d7'],
        ['2e2f31d7', '00000000', '00000000', '00000000', '00000000', '5025c02d', '7e0af1fa'],
        ['7e0af1fa', '00000000', '00000000', '00000000', '00000000', '59c58239', '27cf73c3'],
        ['27cf73c3', 'cf73c327', '8a8f2ecc', '20000000', 'aa8f2ecc', 'de136967', '749c47ab'],
        ['749c47ab', '00000000', '00000000', '00000000', '00000000', '6ccc5a71', '18501dda'],
        ['18501dda', '00000000', '00000000', '00000000', '00000000', 'fa256395', 'e2757e4f'],
        ['e2757e4f', '00000000', '00000000', '00000000', '00000000', '9674ee15', '7401905a'],
        ['7401905a', '00000000', '927c60be', '00000000', '00000000', '5886ca5d', 'cafaaae3'],
        ['cafaaae3', '00000000', '00000000', '00000000', '00000000', '2e2f31d7', 'e4d59b34'],
        ['e4d59b34', '00000000', '00000000', '00000000', '00000000', '7e0af1fa', '9adf6ace'],
        ['9adf6ace', '00000000', '00000000', '00000000', '00000000', '27cf73c3', 'bd10190d'],
        ['bd10190d', '10190dbd', 'cad4d77a', '40000000', '8ad4d77a', '749c47ab', 'fe4890d1'],
        ['fe4890d1', '00000000', '00000000', '00000000', '00000000', '18501dda', 'e6188d0b'],
        ['e6188d0b', '00000000', '00000000', '00000000', '00000000', 'e2757e4f', '046df344'],
        ['046df344', '00000000', '00000000', '00000000', '00000000', '7401905a', '706c631e'],
    ]
# fmt : on

    def check_aes_key_expansion_bytes(self,
                                      key,
                                      rcon,
                                      n_k,
                                      n_r,
                                      n_b,
                                      out_check,):
        """ A unit test function.
        """
        (
            _out_ut,
            _debug_results_ut,
        ) = key_expansion_bytes(key,
                                rcon,
                                n_k,
                                n_r,
                                n_b,)

        print("key len", len(key), key)
        print("temp1_ut len", len(_debug_results_ut["temp1"]), _debug_results_ut["temp1"])
        print("rotword_ut len", len(_debug_results_ut["rotword"]), _debug_results_ut["rotword"])
        print("subword_ut len", len(_debug_results_ut["subword"]), _debug_results_ut["subword"])
        print("temp2_ut len", len(_debug_results_ut["temp2"]), _debug_results_ut["temp2"])
        print("\nout_ut len", len(_out_ut), _out_ut)

        print("\nCheck temp1")
        for _i, _oc in enumerate(out_check):
            _check_word = bytearray.fromhex(out_check[_i][0])
            assert _debug_results_ut["temp1"][_i * 4 + 0:_i * 4 + 4] == _check_word

        print("\nCheck rotword")
        for _i in range(0, len(out_check) // n_k):
            _check_word = bytes.fromhex(out_check[_i * n_k][1])
            # print(i)
            # print(rotword_ut[i * 4 + 0:i * 4 + 4].hex())
            # print(check_word.hex())
            assert _debug_results_ut["rotword"][_i * n_k * 4:_i * n_k * 4 + 4] == _check_word

        print("\nCheck subword")
        for _i in range(0, len(out_check) // n_k):
            _check_word = bytes.fromhex(out_check[_i * n_k][2])
            # print(i)
            # print(subword_ut[i * 4 + 0:i * 4 + 4].hex())
            # print(check_word.hex())
            assert _debug_results_ut["subword"][_i * n_k * 4:_i * n_k * 4 + 4] == _check_word

        print("\nCheck temp2")
        for _i in range(0, len(out_check) // n_k):
            _check_word = bytes.fromhex(out_check[_i * n_k][4])
            # print(i * Nk)
            # print(temp2_ut[i * Nk * 4:i * Nk * 4 + 4].hex())
            # print(check_word.hex())
            assert _debug_results_ut["temp2"][_i * 4 * n_k:_i * 4 * n_k + 4] == _check_word

        print("\nCheck w")
        for _i, _oc in enumerate(out_check):
            _check_word = bytes.fromhex(out_check[_i][6])
            assert _out_ut[_i * 4:_i * 4 + 4] == _check_word


    def check_aes_key_expansion_eic_bytes(self,
                                          key,
                                          rcon,
                                          n_k,
                                          n_r,
                                          n_b,
                                          out_check,):
        """ A unit test function.
        """
        (
            _out_ut,
            _debug_results_ut,
        ) = key_expansion_eic_bytes(key,
                                    rcon,
                                    n_k,
                                    n_r,
                                    n_b,)

        print("key len", len(key), key)
        print("temp1_ut len", len(_debug_results_ut["temp1"]), _debug_results_ut["temp1"])
        print("rotword_ut len", len(_debug_results_ut["rotword"]), _debug_results_ut["rotword"])
        print("subword_ut len", len(_debug_results_ut["subword"]), _debug_results_ut["subword"])
        print("temp2_ut len", len(_debug_results_ut["temp2"]), _debug_results_ut["temp2"])
        print("\nout_ut len", len(_out_ut), _out_ut)

        print("\nCheck temp1")
        for _i, _oc in enumerate(out_check):
            _check_word = bytearray.fromhex(out_check[_i][0])
            assert _debug_results_ut["temp1"][_i * 4 + 0:_i * 4 + 4] == _check_word

        print("\nCheck rotword")
        for _i in range(0, len(out_check) // n_k):
            _check_word = bytes.fromhex(out_check[_i * n_k][1])
            # print(i)
            # print(rotword_ut[i * 4 + 0:i * 4 + 4].hex())
            # print(check_word.hex())
            assert _debug_results_ut["rotword"][_i * n_k * 4:_i * n_k * 4 + 4] == _check_word

        print("\nCheck subword")
        for _i in range(0, len(out_check) // n_k):
            _check_word = bytes.fromhex(out_check[_i * n_k][2])
            # print(i)
            # print(subword_ut[i * 4 + 0:i * 4 + 4].hex())
            # print(check_word.hex())
            assert _debug_results_ut["subword"][_i * n_k * 4:_i * n_k * 4 + 4] == _check_word

        print("\nCheck temp2")
        for _i in range(0, len(out_check) // n_k):
            _check_word = bytes.fromhex(out_check[_i * n_k][4])
            # print(i * Nk)
            # print(temp2_ut[i * Nk * 4:i * Nk * 4 + 4].hex())
            # print(check_word.hex())
            assert _debug_results_ut["temp2"][_i * 4 * n_k:_i * 4 * n_k + 4] == _check_word

        print("\nCheck dw")
        # ToDo, find test vectors for dw in KeyExpansionEIC
        for _i, _oc in enumerate(out_check):
            _check_word = bytes.fromhex(out_check[_i][6])
            # print(i)
            # print(out_ut[i * 4:i * 4 + 4].hex())
            # print(check_word.hex())
            # assert out_ut[i * 4:i * 4 + 4] == check_word

    def test_aes_128_key_expansion_bytes(self):
        """ A unit test function.
        """
        _key = b'\x2b\x7e\x15\x16\x28\xae\xd2\xa6\xab\xf7\x15\x88\x09\xcf\x4f\x3c'
        self.check_aes_key_expansion_bytes(_key,
                                           AES_128_RCON_BYTES,
                                           AES_128_N_K,
                                           AES_128_N_R,
                                           AES_128_N_B,
                                           self._OUT_CHECK_128,)

    def test_aes_192_key_expansion_bytes(self):
        """ A unit test function.
        """
        print("\ntest_aes_192_key_expansion_bytes")
        _key = b'\x8e\x73\xb0\xf7\xda\x0e\x64\x52\xc8\x10\xf3\x2b\x80\x90\x79\xe5\x62\xf8\xea\xd2\x52\x2c\x6b\x7b'
        self.check_aes_key_expansion_bytes(_key,
                                           AES_192_RCON_BYTES,
                                           AES_192_N_K,
                                           AES_192_N_R,
                                           AES_192_N_B,
                                           self._OUT_CHECK_192,)

    def test_aes_256_key_expansion_bytes(self):
        """ A unit test function.
        """
        _key = b'\x60\x3d\xeb\x10\x15\xca\x71\xbe\x2b\x73\xae\xf0\x85\x7d\x77\x81\x1f\x35\x2c\x07\x3b\x61\x08\xd7\x2d\x98\x10\xa3\x09\x14\xdf\xf4'
        self.check_aes_key_expansion_bytes(_key,
                                           AES_256_RCON_BYTES,
                                           AES_256_N_K,
                                           AES_256_N_R,
                                           AES_256_N_B,
                                           self._OUT_CHECK_256,)

    def test_aes_128_key_expansion_eic_bytes(self):
        """ A unit test function.
        """
        _key = b'\x2b\x7e\x15\x16\x28\xae\xd2\xa6\xab\xf7\x15\x88\x09\xcf\x4f\x3c'
        self.check_aes_key_expansion_eic_bytes(_key,
                                               AES_128_RCON_BYTES,
                                               AES_128_N_K,
                                               AES_128_N_R,
                                               AES_128_N_B,
                                               self._OUT_CHECK_128,)

    def test_aes_192_key_expansion_eic_bytes(self):
        """ A unit test function.
        """
        _key = b'\x8e\x73\xb0\xf7\xda\x0e\x64\x52\xc8\x10\xf3\x2b\x80\x90\x79\xe5\x62\xf8\xea\xd2\x52\x2c\x6b\x7b'
        self.check_aes_key_expansion_eic_bytes(_key,
                                               AES_192_RCON_BYTES,
                                               AES_192_N_K,
                                               AES_192_N_R,
                                               AES_192_N_B,
                                               self._OUT_CHECK_192,)

    def test_aes_256_key_expansion_eic_bytes(self):
        """ A unit test function.
        """
        _key = b'\x60\x3d\xeb\x10\x15\xca\x71\xbe\x2b\x73\xae\xf0\x85\x7d\x77\x81\x1f\x35\x2c\x07\x3b\x61\x08\xd7\x2d\x98\x10\xa3\x09\x14\xdf\xf4'
        self.check_aes_key_expansion_eic_bytes(_key,
                                               AES_256_RCON_BYTES,
                                               AES_256_N_K,
                                               AES_256_N_R,
                                               AES_256_N_B,
                                               self._OUT_CHECK_256,)

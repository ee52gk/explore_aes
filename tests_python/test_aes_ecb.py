"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

Ref: https://csrc.nist.gov/pubs/sp/800/38/a/final
Ref: https://csrc.nist.gov/Projects/cryptographic-algorithm-validation-program/Block-Ciphers
Ref: https://pages.nist.gov/ACVP/
Ref: https://nvlpubs.nist.gov/nistpubs/hb/2021/NIST.HB.150-17-2021.pdf
"""

# Standard imports.
import copy
import unittest


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_ecb import (
    aes128_ecb_decrypt,
    aes128_ecb_encrypt,
    aes192_ecb_decrypt,
    aes192_ecb_encrypt,
    aes256_ecb_decrypt,
    aes256_ecb_encrypt,
)


def read_rsp_file(
    filename: str,
) -> tuple[list, list]:
    """Read in the appropriate set of test vector files."""
    with open(filename, "rt", encoding="utf-8") as text_fp:
        _test_vectors = {
            "Count": 0,
            "K": b"",
            "IV": b"",
            "P": b"",
            "C": b"",
        }

        _decrypt_test_vector_set = []
        _encrypt_test_vector_set = []
        _not_eof = "\n"
        _test_vector_set = None
        while _not_eof != "":
            _not_eof = text_fp.readline()
            if _not_eof == "[DECRYPT]\n":
                _test_vector_set = _decrypt_test_vector_set
            if _not_eof == "[ENCRYPT]\n":
                _test_vector_set = _encrypt_test_vector_set
            if _not_eof[0:7] == "COUNT =":
                _test_vector_set.append(copy.deepcopy(_test_vectors))
                _test_vector_set[-1]["Count"] = int(_not_eof[8:])
                _k_text = text_fp.readline()
                _test_vector_set[-1]["K"] = bytes.fromhex(_k_text[6:-1])
                _check_text = text_fp.readline()
                if _check_text[0] == "P":
                    _test_vector_set[-1]["P"] = bytes.fromhex(_check_text[12:-1])
                elif _check_text[0] == "C":
                    _test_vector_set[-1]["C"] = bytes.fromhex(_check_text[13:-1])
                _check_text = text_fp.readline()
                if _check_text[0] == "P":
                    _test_vector_set[-1]["P"] = bytes.fromhex(_check_text[12:-1])
                elif _check_text[0] == "C":
                    _test_vector_set[-1]["C"] = bytes.fromhex(_check_text[13:-1])

        _return = (
            _decrypt_test_vector_set,
            _encrypt_test_vector_set,
        )

        return _return


def aes_ecb_mct_decrypt(
    test_vector_set: list,
    decrypt_func,
):
    """A unit test function."""
    for _test_vectors in test_vector_set:
        _p_ut = _test_vectors["C"]
        for _j in range(0, 1000):
            (_p_ut,) = decrypt_func(_test_vectors["K"], _p_ut)
            # if _j <= 5:
            #    print(_j, _c_ut.hex())

        assert _p_ut == _test_vectors["P"]
        print(_test_vectors["Count"], "Pass")


def aes_ecb_mct_encrypt(
    test_vector_set: list,
    encrypt_func,
):
    """A unit test function."""
    for _test_vectors in test_vector_set:
        _c_ut = _test_vectors["P"]
        for _j in range(0, 1000):
            (_c_ut,) = encrypt_func(
                _test_vectors["K"],
                _c_ut,
            )
            # if _j <= 5:
            #    print(_j, _c_ut.hex())

        assert _c_ut == _test_vectors["C"]
        print(_test_vectors["Count"], "Pass")


class TestAes128ECBMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (
            self._decrypt_test_vector_set,
            self._encrypt_test_vector_set,
        ) = read_rsp_file("./test_vectors/aesmct/ECBMCT128.rsp")

    def test_aes128_ecb_decrypt(self):
        """A unit test function."""
        aes_ecb_mct_decrypt(
            self._decrypt_test_vector_set,
            aes128_ecb_decrypt,
        )

    def test_aes128_ecb_encrypt(self):
        """A unit test function."""
        aes_ecb_mct_encrypt(
            self._encrypt_test_vector_set,
            aes128_ecb_encrypt,
        )


class TestAes192EcbMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (
            self._decrypt_test_vector_set,
            self._encrypt_test_vector_set,
        ) = read_rsp_file("./test_vectors/aesmct/ECBMCT192.rsp")

    def test_aes192_ecb_decrypt(self):
        """A unit test function."""
        aes_ecb_mct_decrypt(
            self._decrypt_test_vector_set,
            aes192_ecb_decrypt,
        )

    def test_aes192_ecb_encrypt(self):
        """A unit test function."""
        aes_ecb_mct_encrypt(
            self._encrypt_test_vector_set,
            aes192_ecb_encrypt,
        )


class TestAes256EcbMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (
            self._decrypt_test_vector_set,
            self._encrypt_test_vector_set,
        ) = read_rsp_file("./test_vectors/aesmct/ECBMCT256.rsp")

    def test_aes256_ecb_decrypt(self):
        """A unit test function."""
        aes_ecb_mct_decrypt(
            self._decrypt_test_vector_set,
            aes256_ecb_decrypt,
        )

    def test_aes256_ecb_encrypt(self):
        """A unit test function."""
        aes_ecb_mct_encrypt(
            self._encrypt_test_vector_set,
            aes256_ecb_encrypt,
        )


class TestAes128EcbVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/ECBMMT128.rsp",
            "./test_vectors/KAT_AES/ECBGFSbox128.rsp",
            "./test_vectors/KAT_AES/ECBKeySbox128.rsp",
            "./test_vectors/KAT_AES/ECBVarKey128.rsp",
            "./test_vectors/KAT_AES/ECBVarTxt128.rsp",
        ]:
            (_decrypt_test_vector_set, _encrypt_test_vector_set) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes128_ecb_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (_p_ut,) = aes128_ecb_decrypt(_test_vectors["K"], _test_vectors["C"])

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes128_ecb_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (_c_ut,) = aes128_ecb_encrypt(_test_vectors["K"], _test_vectors["P"])

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")


class TestAes192EcbVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/ECBMMT192.rsp",
            "./test_vectors/KAT_AES/ECBGFSbox192.rsp",
            "./test_vectors/KAT_AES/ECBKeySbox192.rsp",
            "./test_vectors/KAT_AES/ECBVarKey192.rsp",
            "./test_vectors/KAT_AES/ECBVarTxt192.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes192_ecb_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (_p_ut,) = aes192_ecb_decrypt(
                    _test_vectors["K"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes192_ecb_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (_c_ut,) = aes192_ecb_encrypt(
                    _test_vectors["K"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")


class TestAes256EcbVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/ECBMMT256.rsp",
            "./test_vectors/KAT_AES/ECBGFSbox256.rsp",
            "./test_vectors/KAT_AES/ECBKeySbox256.rsp",
            "./test_vectors/KAT_AES/ECBVarKey256.rsp",
            "./test_vectors/KAT_AES/ECBVarTxt256.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes256_ecb_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (_p_ut,) = aes256_ecb_decrypt(
                    _test_vectors["K"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes256_ecb_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (_c_ut,) = aes256_ecb_encrypt(
                    _test_vectors["K"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")

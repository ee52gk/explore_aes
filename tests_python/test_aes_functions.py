"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://csrc.nist.gov/pubs/fips/197/final
Ref: https://csrc.nist.gov/projects/cryptographic-algorithm-validation-program/block-ciphers#AES
"""

# Standard imports.
import json
import unittest


# 3rd party imports.
import pytest


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_128_RCON_BYTES,
    AES_128_N_B,
    AES_128_N_K,
    AES_128_N_R,
    AES_192_RCON_BYTES,
    AES_192_N_B,
    AES_192_N_K,
    AES_192_N_R,
    AES_256_RCON_BYTES,
    AES_256_N_B,
    AES_256_N_K,
    AES_256_N_R,
)
from explore_aes_python.aes_key_expansion import (
    key_expansion_bytes,
    key_expansion_eic_bytes,
)
from explore_aes_python.aes_functions import (
    x_times_b,
    gmul_x_0x02,
    gmul_x_0x03,
    gmul_x_0x09,
    gmul_x_0x0b,
    gmul_x_0x0d,
    gmul_x_0x0e,
    gmul_by_0x02,
    gmul_by_0x03,
    gmul_by_0x09,
    gmul_by_0x0b,
    gmul_by_0x0d,
    gmul_by_0x0e,
    mix_columns_bytes,
    inv_mix_columns_bytes,
    cipher_bytes,
    inv_cipher_bytes,
    eq_inv_cipher_bytes,
)


class TestAesFunctions:
    """A unit test class."""

    def test_x_times_b(self):
        """A unit test function."""
        assert x_times_b(0x57) == 0xAE
        assert x_times_b(0xAE) == 0x47
        assert x_times_b(0x47) == 0x8E
        assert x_times_b(0x8E) == 0x07
        assert x_times_b(0x07) == 0x0E
        assert x_times_b(0x0E) == 0x1C
        assert x_times_b(0x1C) == 0x38

    def test_gmul_x_(self):
        """A unit test function."""
        for _b in range(0, 256):
            assert gmul_x_0x02(_b) == gmul_by_0x02[_b]
            assert gmul_x_0x03(_b) == gmul_by_0x03[_b]
            assert gmul_x_0x09(_b) == gmul_by_0x09[_b]
            assert gmul_x_0x0b(_b) == gmul_by_0x0b[_b]
            assert gmul_x_0x0d(_b) == gmul_by_0x0d[_b]
            assert gmul_x_0x0e(_b) == gmul_by_0x0e[_b]

    def test_mix_columns_bytes(self):
        """A unit test function."""
        _check_in = [
            b"\xdb\x13\x53\x45",
            b"\xf2\x0a\x22\x5c",
            b"\x01\x01\x01\x01",
            b"\xc6\xc6\xc6\xc6",
            b"\xd4\xd4\xd4\xd5",
            b"\x2d\x26\x31\x4c",
        ]

        _check_out = [
            b"\x8e\x4d\xa1\xbc",
            b"\x9f\xdc\x58\x9d",
            b"\x01\x01\x01\x01",
            b"\xc6\xc6\xc6\xc6",
            b"\xd5\xd5\xd7\xd6",
            b"\x4d\x7e\xbd\xf8",
        ]

        for _i, _ci in enumerate(_check_in):
            _state_in0 = bytearray(4)
            _state_in1 = bytearray(4)
            _state_in2 = bytearray(4)
            _state_in3 = bytearray(4)

            _state_in0 = _check_in[_i]

            _state_in = _state_in0 + _state_in1 + _state_in2 + _state_in3

            _out_ut = mix_columns_bytes(_state_in)

            assert _out_ut[0 * 4 : 0 * 4 + 4] == _check_out[_i]

            _state_in2 = _check_in[_i]

            _state_in = _state_in0 + _state_in1 + _state_in2 + _state_in3

            _out_ut = mix_columns_bytes(_state_in)

            assert _out_ut[2 * 4 : 2 * 4 + 4] == _check_out[_i]

    def test_inv_mix_columns_bytes(self):
        """A unit test function."""
        _check_in = [
            b"\xdb\x13\x53\x45",
            b"\xf2\x0a\x22\x5c",
            b"\x01\x01\x01\x01",
            b"\xc6\xc6\xc6\xc6",
            b"\xd4\xd4\xd4\xd5",
            b"\x2d\x26\x31\x4c",
        ]

        _check_out = [
            b"\x8e\x4d\xa1\xbc",
            b"\x9f\xdc\x58\x9d",
            b"\x01\x01\x01\x01",
            b"\xc6\xc6\xc6\xc6",
            b"\xd5\xd5\xd7\xd6",
            b"\x4d\x7e\xbd\xf8",
        ]

        for _i, _ci in enumerate(_check_in):
            _state_in0 = bytearray(4)
            _state_in1 = bytearray(4)
            _state_in2 = bytearray(4)
            _state_in3 = bytearray(4)

            _state_in0 = _check_out[_i]

            _state_in = _state_in0 + _state_in1 + _state_in2 + _state_in3

            _out_ut = inv_mix_columns_bytes(_state_in)

            assert _out_ut[0 * 4 : 0 * 4 + 4] == _check_in[_i]

            _state_in2 = _check_out[_i]

            _state_in = _state_in0 + _state_in1 + _state_in2 + _state_in3

            _out_ut = inv_mix_columns_bytes(_state_in)

            assert _out_ut[2 * 4 : 2 * 4 + 4] == _check_in[_i]


class TestAes128CipherBytes:
    """A unit test class."""

    def aes_cipher_bytes(
        self,
        aes_ecb_test_vectors,
        rcon: list,
        n_k: int,
        n_r: int,
        n_b: int,
    ):
        """A unit test helper function."""
        _key_in = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Key"])
        _pt_in = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Plaintext"])
        _ct_check = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Ciphertext"])

        print("key_in         ", len(_key_in), _key_in.hex())
        print("pt_in          ", len(_pt_in), _pt_in.hex())
        print("ct_check       ", len(_ct_check), _ct_check.hex())

        (
            _w,
            _,
        ) = key_expansion_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )
        for _b in range(0, len(_pt_in) // (n_b * 4)):
            _block_in = _pt_in[_b * n_b * 4 : (_b + 1) * n_b * 4]

            (
                _block_ut,
                _debug_results_ut,
            ) = cipher_bytes(
                _block_in,
                _w,
                n_k,
                n_r,
                n_b,
            )

            print("ct_check          ", _ct_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex())
            print("block_eARK        ", _block_ut.hex())
            assert _block_ut.hex() == _ct_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex()

            _ct_check_s_ark = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["sKeyAddition"])
            print("ct_sARK_check     ", _ct_check_s_ark.hex())
            print("block_sARK        ", _debug_results_ut["block_sARK"].hex())
            assert _debug_results_ut["block_sARK"].hex() == _ct_check_s_ark.hex()

            for r in range(0, n_r - 1):
                _round_sb_check = bytes.fromhex(
                    aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["Round = " + str(r + 1)]["Substitution"]
                )
                print("round_SB_check    ", _round_sb_check)
                print("round_SB[r]     ", r, _debug_results_ut["round_SB"][r].hex())
                assert _debug_results_ut["round_SB"][r].hex() == _round_sb_check.hex()

                _round_sr_check = bytes.fromhex(
                    aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["Round = " + str(r + 1)]["ShiftRow"]
                )
                print("round_SR_check    ", _round_sr_check)
                print("round_SR[r]     ", r, _debug_results_ut["round_SR"][r].hex())
                assert _debug_results_ut["round_SR"][r].hex() == _round_sr_check.hex()

                _round_mc_check = bytes.fromhex(
                    aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["Round = " + str(r + 1)]["MixColumn"]
                )
                print("round_MC_check    ", _round_mc_check)
                print("round_MC[r]     ", r, _debug_results_ut["round_MC"][r].hex())
                # assert debug_results_ut["round_MC"][r].hex() == round_MC_check.hex()

                _round_ark_check = bytes.fromhex(
                    aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["Round = " + str(r + 1)]["KeyAddition"]
                )
                print("round_ARK_check   ", _round_ark_check)
                print("round_ARK[r]    ", r, _debug_results_ut["round_ARK"][r].hex())
                assert _debug_results_ut["round_ARK"][r].hex() == _round_ark_check.hex()

            _ct_check_e_sb = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["eSubstitution"])
            print("ct_eSB_check      ", _ct_check_e_sb.hex())
            print("block_eSB         ", _debug_results_ut["block_eSB"].hex())
            assert _debug_results_ut["block_eSB"].hex() == _ct_check_e_sb.hex()

            _ct_check_e_sr = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["eShiftRow"])
            print("ct_eSR_check      ", _ct_check_e_sr.hex())
            print("block_eSR         ", _debug_results_ut["block_eSR"].hex())
            assert _debug_results_ut["block_eSR"].hex() == _ct_check_e_sr.hex()

            _ct_check_e_ark = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Block " + str(_b + 1)]["eKeyAddition"])
            print("ct_eARK_check     ", _ct_check_e_ark.hex())
            print("block_eARK        ", _block_ut.hex())
            assert _debug_results_ut["block_eARK"].hex() == _ct_check_e_ark.hex()

            print("ct_check          ", _ct_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex())
            print("block_eARK        ", _block_ut.hex())
            assert _block_ut.hex() == _ct_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex()

    def test_aes_128_cipher_bytes(self):
        """A unit test function."""
        with open(
            "test_vectors/aes_128_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_128_ecb_test_vectors = json.load(json_data)
        self.aes_cipher_bytes(
            _aes_128_ecb_test_vectors["ECB-AES128"],
            AES_128_RCON_BYTES,
            AES_128_N_K,
            AES_128_N_R,
            AES_128_N_B,
        )

    def test_aes_192_cipher_bytes(self):
        """A unit test function."""
        with open(
            "test_vectors/aes_192_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_192_ecb_test_vectors = json.load(json_data)
        self.aes_cipher_bytes(
            _aes_192_ecb_test_vectors["ECB-AES192"],
            AES_192_RCON_BYTES,
            AES_192_N_K,
            AES_192_N_R,
            AES_192_N_B,
        )

    def test_aes_256_cipher_bytes(self):
        """A unit test function."""
        with open(
            "test_vectors/aes_256_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_256_ecb_test_vectors = json.load(json_data)
        self.aes_cipher_bytes(
            _aes_256_ecb_test_vectors["ECB-AES256"],
            AES_256_RCON_BYTES,
            AES_256_N_K,
            AES_256_N_R,
            AES_256_N_B,
        )


class TestAesInvCipherBytes:
    """A unit test class."""

    def aes_inv_cipher_bytes(
        self,
        aes_ecb_test_vectors,
        rcon: list,
        n_k: int,
        n_r: int,
        n_b: int,
    ):
        """A unit test helper function."""
        _key_in = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Key"])
        _ct_in = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Ciphertext"])
        _pt_check = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Plaintext"])

        print("key_in         ", len(_key_in), _key_in.hex())
        print("ct_in          ", len(_ct_in), _ct_in.hex())
        print("pt_check       ", len(_pt_check), _pt_check.hex())

        (
            _w,
            _,
        ) = key_expansion_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )
        for _b in range(0, len(_ct_in) // (n_b * 4)):
            _block_in = _ct_in[_b * n_b * 4 : (_b + 1) * n_b * 4]
            print("block_in             ", _block_in.hex())

            (
                _block_ut,
                _debug_results_ut,
            ) = inv_cipher_bytes(
                _block_in,
                _w,
                n_k,
                n_r,
                n_b,
            )

            _ct_check_s_ka = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["sKeyAddition"])
            print("ct_check_sKA         ", _ct_check_s_ka.hex())
            print("block_ut_sARK        ", _debug_results_ut["block_sARK"].hex())
            assert _debug_results_ut["block_sARK"].hex() == _ct_check_s_ka.hex()

            _ct_check_s_sr = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["sSubstitution"])
            print("ct_check_sSR         ", _ct_check_s_sr.hex())
            print("round_ISR[round]   ", 0, _debug_results_ut["round_ISR"][0].hex())
            # assert debug_results_ut["round_ISR"][0].hex() == ct_check_sSR.hex()

            _ct_check_s_sb = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["sShiftRow"])
            print("ct_check_sS          ", _ct_check_s_sb.hex())
            print("round_ISB[round]   ", 0, _debug_results_ut["round_ISB"][0].hex())
            assert _debug_results_ut["round_ISB"][0].hex() == _ct_check_s_sb.hex()

            for _r_rev in range(n_r - 1, 0, -1):
                _r_fwd = n_r - 1 - _r_rev

                _round_ark_check = bytes.fromhex(
                    aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["Round = " + str(_r_rev)]["KeyAddition"]
                )
                print("round_ARK_check    ", _r_fwd, _round_ark_check)
                print("round_ut_ARK[round]", _r_fwd, _debug_results_ut["round_ARK"][_r_fwd].hex())
                assert _debug_results_ut["round_ARK"][_r_fwd].hex() == _round_ark_check.hex()

                _round_imc_check = bytes.fromhex(
                    aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["Round = " + str(_r_rev)][
                        "InvMixColumn"
                    ]
                )
                print("round_IMC_check    ", _r_fwd, _round_imc_check)
                print("round_ut_IMC[round]", _r_fwd, _debug_results_ut["round_IMC"][_r_fwd].hex())
                assert _debug_results_ut["round_IMC"][_r_fwd].hex() == _round_imc_check.hex()

                if _r_rev < 8:
                    _round_isr_check = bytes.fromhex(
                        aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["Round = " + str(_r_fwd)][
                            "Substitution"
                        ]
                    )
                    print("round_ISR_check    ", _r_fwd, _round_isr_check)
                    print("round_ut_ISR[round]", (_r_fwd + 1) % 8, _debug_results_ut["round_ISR"][(_r_rev + 1)].hex())
                    # assert debug_results_ut["round_ISR"][r + 1].hex() == round_ISR_check.hex()

                    _round_isb_check = bytes.fromhex(
                        aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["Round = " + str(_r_fwd)][
                            "ShiftRow"
                        ]
                    )
                    print("round_ISB_check    ", _r_fwd, _round_isb_check)
                    print("round_ut_ISB[round]", (_r_fwd + 1) % 8, _debug_results_ut["round_ISB"][(_r_rev + 1)].hex())
                    assert _debug_results_ut["round_ISB"][_r_rev + 1].hex() == _round_isb_check.hex()

            _pt_check_e_ka = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Block " + str(_b + 1)]["eKeyAddition"])
            print("pt_check_eKA ", _pt_check_e_ka.hex())
            print("block_ut_eARK", _block_ut.hex())
            assert _block_ut.hex() == _pt_check_e_ka.hex()

            print("pt_check     ", _pt_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex())
            print("block_ut_eKA ", _block_ut.hex())
            assert _block_ut.hex() == _pt_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex()

    def test_aes_128_inv_cipher_bytes(self):
        """A unit test function."""
        with open(
            "./test_vectors/aes_128_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_128_ecb_test_vectors = json.load(json_data)
        self.aes_inv_cipher_bytes(
            _aes_128_ecb_test_vectors["ECB-AES128"],
            AES_128_RCON_BYTES,
            AES_128_N_K,
            AES_128_N_R,
            AES_128_N_B,
        )

    def test_aes_192_inv_cipher_bytes(self):
        """A unit test function."""
        with open(
            "./test_vectors/aes_192_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_192_ecb_test_vectors = json.load(json_data)
        self.aes_inv_cipher_bytes(
            _aes_192_ecb_test_vectors["ECB-AES192"],
            AES_192_RCON_BYTES,
            AES_192_N_K,
            AES_192_N_R,
            AES_192_N_B,
        )

    def test_aes_256_inv_cipher_bytes(self):
        """A unit test function."""
        with open(
            "./test_vectors/aes_256_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_256_ecb_test_vectors = json.load(json_data)
        self.aes_inv_cipher_bytes(
            _aes_256_ecb_test_vectors["ECB-AES256"],
            AES_256_RCON_BYTES,
            AES_256_N_K,
            AES_256_N_R,
            AES_256_N_B,
        )


class TestAesEqInvCipherBytes:
    """A unit test class."""

    def aes_eq_inv_cipher_bytes(
        self,
        aes_ecb_test_vectors: dict,
        rcon: int,
        n_k: int,
        n_r: int,
        n_b: int,
    ):
        """A unit test helper function."""
        _key_in = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Key"])
        _ct_in = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Ciphertext"])
        _pt_check = bytes.fromhex(aes_ecb_test_vectors["Decryption"]["Plaintext"])

        print("key_in         ", len(_key_in), _key_in.hex())
        print("ct_in          ", len(_ct_in), _ct_in.hex())
        print("pt_check       ", len(_pt_check), _pt_check.hex())

        (
            _dw,
            _,
        ) = key_expansion_eic_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )

        for _b in range(0, len(_ct_in) // (n_b * 4)):
            _block_in = _ct_in[_b * n_b * 4 : (_b + 1) * n_b * 4]

            (
                _block_ut,
                _,
            ) = eq_inv_cipher_bytes(
                _block_in,
                _dw,
                n_k,
                n_r,
                n_b,
            )

            print("pt_check", _pt_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex())
            print("block_ut", _block_ut.hex())
            assert _block_ut.hex() == _pt_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex()

    def test_aes_128_eq_inv_cipher_bytes(self):
        """A unit test function."""
        with open(
            "./test_vectors/aes_128_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_128_ecb_test_vectors = json.load(json_data)
        self.aes_eq_inv_cipher_bytes(
            _aes_128_ecb_test_vectors["ECB-AES128"],
            AES_128_RCON_BYTES,
            AES_128_N_K,
            AES_128_N_R,
            AES_128_N_B,
        )

    def test_aes_192_eq_inv_cipher_bytes(self):
        """A unit test function."""
        print("\ntest_aes_192_eq_inv_cipher_bytes")
        with open(
            "./test_vectors/aes_192_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_192_ecb_test_vectors = json.load(json_data)
        self.aes_eq_inv_cipher_bytes(
            _aes_192_ecb_test_vectors["ECB-AES192"],
            AES_192_RCON_BYTES,
            AES_192_N_K,
            AES_192_N_R,
            AES_192_N_B,
        )

    def test_aes_256_eq_inv_cipher_bytes(self):
        """A unit test function."""
        print("\ntest_aes_256_eq_inv_cipher_bytes")
        with open(
            "./test_vectors/aes_256_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            _aes_256_ecb_test_vectors = json.load(json_data)
        self.aes_eq_inv_cipher_bytes(
            _aes_256_ecb_test_vectors["ECB-AES256"],
            AES_256_RCON_BYTES,
            AES_256_N_K,
            AES_256_N_R,
            AES_256_N_B,
        )


class TestAesNegativeCasesBytes:
    """A unit test class."""

    @pytest.mark.xfail
    @unittest.expectedFailure
    def test_key_expansion_wrong_key_length(self):
        """A unit test  function."""
        # _key_in = bytes.fromhex("139a35422f1d61de3c91787fe0507afd")
        _key_in = bytes.fromhex("139a35422f1d61de3c91787fe0507afd11")
        _pt_in = bytes.fromhex("b9145a768b7dc489a096b546f43b231f")
        _ct_check = bytes.fromhex("d7c3ffac9031238650901e157364c386")

        rcon = AES_128_RCON_BYTES
        n_k = AES_128_N_K
        n_r = AES_128_N_R
        n_b = AES_128_N_B

        (
            _w,
            _,
        ) = key_expansion_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )

    @pytest.mark.xfail
    @unittest.expectedFailure
    def test_key_expansion_eic_wrong_key_length(self):
        """A unit test  function."""
        # _key_in = bytes.fromhex("139a35422f1d61de3c91787fe0507afd")
        _key_in = bytes.fromhex("139a35422f1d61de3c91787fe0507afd1122")
        _pt_in = bytes.fromhex("b9145a768b7dc489a096b546f43b231f")
        _ct_check = bytes.fromhex("d7c3ffac9031238650901e157364c386")

        rcon = AES_192_RCON_BYTES
        n_k = AES_192_N_K
        n_r = AES_192_N_R
        n_b = AES_192_N_B

        (
            _w,
            _,
        ) = key_expansion_eic_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )

    @pytest.mark.xfail
    @unittest.expectedFailure
    def test_cipher_wrong_block_length(self):
        """A unit test  function."""
        _key_in = bytes.fromhex("139a35422f1d61de3c91787fe0507afd139a35422f1d61de")
        # _pt_in = bytes.fromhex("b9145a768b7dc489a096b546f43b231f")
        _pt_in = bytes.fromhex("b9145a768b7dc489a096b546f43b231f11")
        _ct_check = bytes.fromhex("d7c3ffac9031238650901e157364c386")

        rcon = AES_192_RCON_BYTES
        n_k = AES_192_N_K
        n_r = AES_192_N_R
        n_b = AES_192_N_B

        (
            _w,
            _,
        ) = key_expansion_eic_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )

        (
            _block_ut,
            _,
        ) = cipher_bytes(
            _pt_in,
            _w,
            n_k,
            n_r,
            n_b,
        )

        print("pt_check", _ct_check.hex())
        print("block_ut", _block_ut.hex())
        assert _block_ut.hex() == _ct_check.hex()

    @pytest.mark.xfail
    @unittest.expectedFailure
    def test_aes_arbitrary_bytes(self):
        """A unit test  function."""
        _key_in = bytes.fromhex("edfdb257cb37cdf182c5455b0c0efebb")
        _pt_in = bytes.fromhex("1695fe475421cace3557daca01f445ff")
        # _ct_check = bytes.fromhex("7888beae6e7a426332a7eaa2f808e637")
        _ct_check = bytes.fromhex("7888beae6e7a426332a7eaa2f808e638")

        rcon = AES_128_RCON_BYTES
        n_k = AES_128_N_K
        n_r = AES_128_N_R
        n_b = AES_128_N_B

        print("key_in         ", len(_key_in), _key_in.hex())
        print("pt_in          ", len(_pt_in), _pt_in.hex())
        print("ct_check       ", len(_ct_check), _ct_check.hex())

        (
            _w,
            _,
        ) = key_expansion_bytes(
            _key_in,
            rcon,
            n_k,
            n_r,
            n_b,
        )

        for _b in range(0, len(_pt_in) // (n_b * 4)):
            _block_in = _pt_in[_b * n_b * 4 : (_b + 1) * n_b * 4]

            (
                _block_ut,
                _,
            ) = cipher_bytes(
                _block_in,
                _w,
                n_k,
                n_r,
                n_b,
            )

            print("pt_check", _ct_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex())
            print("block_ut", _block_ut.hex())
            assert _block_ut.hex() == _ct_check[_b * 4 * 4 : _b * 4 * 4 + 4 * 4].hex()

"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://csrc.nist.gov/pubs/fips/197/final
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_256_N_K,
    AES_192_N_K,
    AES_128_N_K,
    AES_256_N_B,
    AES_192_N_B,
    AES_128_N_B,
    AES_256_N_R,
    AES_192_N_R,
    AES_128_N_R,
    aes_round_constants_bytes,
    AES_256_NUM_ROUND_CONSTANTS,
    AES_192_NUM_ROUND_CONSTANTS,
    AES_128_NUM_ROUND_CONSTANTS,
    SBOX,
    INV_SBOX,
    sbox_int,
    inv_sbox_int,
)


# Taken from Wikipedia; https://en.wikipedia.org/wiki/AES_key_schedule
# Pre-fixed with 0x00 in position i=0
RCON_CHECK = [0x00, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36]


class TestAesConstants:
    """A unit test class."""

    def test_aes_constants(self):
        """A unit test function."""
        assert AES_256_N_K == 8
        assert AES_192_N_K == 6
        assert AES_128_N_K == 4
        assert AES_256_N_B == 4
        assert AES_192_N_B == 4
        assert AES_128_N_B == 4
        assert AES_256_N_R == 14
        assert AES_192_N_R == 12
        assert AES_128_N_R == 10


class TestAesConstantFunctions:
    """A unit test class."""

    def test_sbox_int(self):
        """A unit test function."""
        for _i in range(0, 16):
            for _j in range(0, 16):
                assert sbox_int((_i * 16) + _j) == SBOX[_i][_j]

    def test_inv_sbox_int(self):
        """A unit test function."""
        print("\ntest_inv_sbox_int")
        for _i in range(0, 16):
            for _j in range(0, 16):
                assert inv_sbox_int((_i * 16) + _j) == INV_SBOX[_i][_j]


class TestAesRoundConstants:
    """A unit test class."""

    def test_aes_128_round_constants_bytes(self):
        """A unit test function."""
        _rcon_ut = aes_round_constants_bytes(AES_128_NUM_ROUND_CONSTANTS)

        assert AES_128_NUM_ROUND_CONSTANTS == 10
        assert len(_rcon_ut) == (AES_128_NUM_ROUND_CONSTANTS + 1) * 4
        for _i in range(1, AES_128_NUM_ROUND_CONSTANTS):
            print(RCON_CHECK[_i])
            assert _rcon_ut[_i * 4] == RCON_CHECK[_i]
            assert _rcon_ut[_i * 4 + 1 : _i * 4 + 4] == bytearray.fromhex("000000")

    def test_aes_192_round_constants_bytes(self):
        """A unit test function."""
        _rcon_ut = aes_round_constants_bytes(AES_192_NUM_ROUND_CONSTANTS)

        assert AES_192_NUM_ROUND_CONSTANTS == 8
        assert len(_rcon_ut) == (AES_192_NUM_ROUND_CONSTANTS + 1) * 4
        for _i in range(1, AES_192_NUM_ROUND_CONSTANTS):
            print(RCON_CHECK[_i])
            assert _rcon_ut[_i * 4] == RCON_CHECK[_i]
            assert _rcon_ut[_i * 4 + 1 : _i * 4 + 4] == bytearray.fromhex("000000")

    def test_aes_256_round_constants_bytes(self):
        """A unit test function."""
        _rcon_ut = aes_round_constants_bytes(AES_256_NUM_ROUND_CONSTANTS)

        assert AES_256_NUM_ROUND_CONSTANTS == 7
        assert len(_rcon_ut) == (AES_256_NUM_ROUND_CONSTANTS + 1) * 4
        for _i in range(1, AES_256_NUM_ROUND_CONSTANTS):
            print(RCON_CHECK[_i])
            assert _rcon_ut[_i * 4] == RCON_CHECK[_i]
            assert _rcon_ut[_i * 4 + 1 : _i * 4 + 4] == bytearray.fromhex("000000")

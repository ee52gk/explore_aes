"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

# Ref: https://csrc.nist.gov/pubs/sp/800/38/a/final
# Ref: https://csrc.nist.gov/Projects/cryptographic-algorithm-validation-program/Block-Ciphers
# Ref: https://pages.nist.gov/ACVP/draft-celi-acvp-symmetric.html#name-test-types
# Ref: https://stackoverflow.com/questions/69588373/aes-cbc-decryption-validation-for-monte-carlo-tests
# Ref: https://stackoverflow.com/questions/65878005/what-is-missing-from-the-aes-validation-standard-pseudocode-for-the-monte-carlo
"""

# Standard imports.
import copy
import unittest


# 3rd party imports.
import pytest


# Submodule imports.


# Local imports.
from explore_aes_python.aes_cbc import (
    aes128_cbc_decrypt,
    aes128_cbc_encrypt,
    aes192_cbc_decrypt,
    aes192_cbc_encrypt,
    aes256_cbc_decrypt,
    aes256_cbc_encrypt,
)


def read_rsp_file(
    filename: str,
) -> tuple[list, list]:
    """Read in the appropriate set of test vector files."""
    with open(
        filename,
        "rt",
        encoding="utf-8",
    ) as text_fp:
        _test_vectors = {
            "Count": 0,
            "K": b"",
            "IV": b"",
            "P": b"",
            "C": b"",
        }

        _decrypt_test_vector_set = []
        _encrypt_test_vector_set = []
        _not_eof = "\n"
        _test_vector_set = None
        while _not_eof != "":
            _not_eof = text_fp.readline()
            if _not_eof == "[DECRYPT]\n":
                _test_vector_set = _decrypt_test_vector_set
            if _not_eof == "[ENCRYPT]\n":
                _test_vector_set = _encrypt_test_vector_set
            if _not_eof[0:7] == "COUNT =":
                _test_vector_set.append(copy.deepcopy(_test_vectors))
                _test_vector_set[-1]["Count"] = int(_not_eof[8:])
                _k_text = text_fp.readline()
                _test_vector_set[-1]["K"] = bytes.fromhex(_k_text[6:-1])
                _i_text = text_fp.readline()
                _test_vector_set[-1]["IV"] = bytes.fromhex(_i_text[5:-1])
                _check_text = text_fp.readline()
                if _check_text[0] == "P":
                    _test_vector_set[-1]["P"] = bytes.fromhex(_check_text[12:-1])
                elif _check_text[0] == "C":
                    _test_vector_set[-1]["C"] = bytes.fromhex(_check_text[13:-1])
                _check_text = text_fp.readline()
                if _check_text[0] == "P":
                    _test_vector_set[-1]["P"] = bytes.fromhex(_check_text[12:-1])
                elif _check_text[0] == "C":
                    _test_vector_set[-1]["C"] = bytes.fromhex(_check_text[13:-1])

        _return = (
            _decrypt_test_vector_set,
            _encrypt_test_vector_set,
        )

        return _return


def aes_cbc_mct_decrypt(
    test_vector_set: list,
    decrypt_func,
):
    """A unit test function."""
    for _test_vectors in test_vector_set:
        # Note that progression across the 999 iterations is not clearly defined in the
        # reference. It implies that the 2nd iteration CT value is the IV from the first
        # iteration. However it does not state what the IV should be for the 2nd iteration
        # onwards. Trial end arror, helped by some StackOverflow references, shows the
        # progression is:
        #   0: K, IV,  C -> P0
        #   1: K,  C, IV -> P1  From the 'CT' value rotates to 'IV'...
        #   2: K, IV, P0 -> P2  ...the 'PT' value two rounds earlier becomes 'CT'.
        #   3: K, P0, P1 -> P3
        #   etc.
        # This works, but appears different to the reference description, and from the
        # progression of the equivalent encrypt test.
        for _j in range(0, 1000):
            if _j == 0:
                (
                    _p_ut_j,
                    _debug_values,
                ) = decrypt_func(_test_vectors["K"], _test_vectors["IV"], _test_vectors["C"])
                _p_ut_jm3 = _test_vectors["C"]
                _p_ut_jm2 = _test_vectors["IV"]
                _p_ut_jm1 = _p_ut_j
            else:
                (
                    _p_ut_j,
                    _debug_values,
                ) = decrypt_func(_test_vectors["K"], _p_ut_jm3, _p_ut_jm2)
                _p_ut_jm3 = _p_ut_jm2
                _p_ut_jm2 = _p_ut_jm1
                _p_ut_jm1 = _p_ut_j
            if _j < 5:
                print(_j, _p_ut_j.hex())

        print(_j, _p_ut_j.hex())
        assert _p_ut_j.hex() == _test_vectors["P"].hex()
        print(_test_vectors["Count"], "Pass")


def aes_cbc_mct_encrypt(
    test_vector_set: list,
    encrypt_func,
):
    """A unit test function."""
    for _test_vectors in test_vector_set:
        # Note that progression across the 999 iterations is not clearly defined in the
        # reference. It implies that the 2nd iteration CT value is the IV from the first
        # iteration. However it does not state what the IV should be for the 2nd iteration
        # onwards. Trial end arror, helped by some StackOverflow references, shows the
        # progression is:
        #   0: K, IV,  C -> P0
        #   1: K,  C, IV -> P1  From the 'CT' value rotates to 'IV'...
        #   2: K, IV, P0 -> P2  ...the 'PT' value two rounds earlier becomes 'CT'.
        #   3: K, P0, P1 -> P3
        #   etc.
        # This works, but appears different to the reference description, and from the
        # progression of the equivalent encrypt test.
        for _j in range(0, 1000):
            if _j == 0:
                (
                    _c_ut_j,
                    _debug_values,
                ) = encrypt_func(_test_vectors["K"], _test_vectors["IV"], _test_vectors["P"])
                _c_ut_jm2 = _test_vectors["IV"]
                _c_ut_jm1 = _c_ut_j
            else:
                (
                    _c_ut_j,
                    _debug_values,
                ) = encrypt_func(_test_vectors["K"], _c_ut_jm2, _c_ut_jm1)
                _c_ut_jm2 = _c_ut_jm1
                _c_ut_jm1 = _c_ut_j
            if _j < 5:
                print(_j, _c_ut_j.hex())

        print(_j, _c_ut_j.hex())
        assert _c_ut_j.hex() == _test_vectors["C"].hex()
        print(_test_vectors["Count"], "Pass")


class TestAes128CbcMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (self._decrypt_test_vector_set, self._encrypt_test_vector_set) = read_rsp_file(
            "./test_vectors/aesmct/CBCMCT128.rsp"
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes128_cbc_decrypt(self):
        """A unit test function."""
        aes_cbc_mct_decrypt(
            self._encrypt_test_vector_set,
            aes128_cbc_decrypt,
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes128_cbc_encrypt(self):
        """A unit test function."""
        aes_cbc_mct_encrypt(
            self._decrypt_test_vector_set,
            aes128_cbc_encrypt,
        )

    def test_aes128_cbc_decrypt(self):
        """A unit test function."""
        aes_cbc_mct_decrypt(
            self._decrypt_test_vector_set,
            aes128_cbc_decrypt,
        )

    def test_aes128_cbc_encrypt(self):
        """A unit test function."""
        aes_cbc_mct_encrypt(
            self._encrypt_test_vector_set,
            aes128_cbc_encrypt,
        )


class TestAes192CbcMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (self._decrypt_test_vector_set, self._encrypt_test_vector_set) = read_rsp_file(
            "./test_vectors/aesmct/CBCMCT192.rsp"
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes192_cbc_decrypt(self):
        """A unit test function."""
        aes_cbc_mct_decrypt(
            self._encrypt_test_vector_set,
            aes192_cbc_decrypt,
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes192_cbc_encrypt(self):
        """A unit test function."""
        aes_cbc_mct_encrypt(
            self._decrypt_test_vector_set,
            aes192_cbc_encrypt,
        )

    def test_aes192_cbc_decrypt(self):
        """A unit test function."""
        aes_cbc_mct_decrypt(
            self._decrypt_test_vector_set,
            aes192_cbc_decrypt,
        )

    def test_aes192_cbc_encrypt(self):
        """A unit test function."""
        aes_cbc_mct_encrypt(
            self._encrypt_test_vector_set,
            aes192_cbc_encrypt,
        )


class TestAes256CbcMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (self._decrypt_test_vector_set, self._encrypt_test_vector_set) = read_rsp_file(
            "./test_vectors/aesmct/CBCMCT256.rsp"
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes256_cbc_decrypt(self):
        """A unit test function."""
        aes_cbc_mct_decrypt(
            self._encrypt_test_vector_set,
            aes256_cbc_decrypt,
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes256_cbc_encrypt(self):
        """A unit test function."""
        aes_cbc_mct_encrypt(
            self._decrypt_test_vector_set,
            aes256_cbc_encrypt,
        )

    def test_aes256_cbc_decrypt(self):
        """A unit test function."""
        aes_cbc_mct_decrypt(
            self._decrypt_test_vector_set,
            aes256_cbc_decrypt,
        )

    def test_aes256_cbc_encrypt(self):
        """A unit test function."""
        aes_cbc_mct_encrypt(
            self._encrypt_test_vector_set,
            aes256_cbc_encrypt,
        )


class TestAes128CbcVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/CBCMMT128.rsp",
            "./test_vectors/KAT_AES/CBCGFSbox128.rsp",
            "./test_vectors/KAT_AES/CBCKeySbox128.rsp",
            "./test_vectors/KAT_AES/CBCVarKey128.rsp",
            "./test_vectors/KAT_AES/CBCVarTxt128.rsp",
        ]:
            (_decrypt_test_vector_set, _encrypt_test_vector_set) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes128_cbc_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _p_ut,
                    _debug_results_ut,
                ) = aes128_cbc_decrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes128_cbc_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _c_ut,
                    _debug_results_ut,
                ) = aes128_cbc_encrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")


class TestAes192CbcVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/CBCMMT192.rsp",
            "./test_vectors/KAT_AES/CBCGFSbox192.rsp",
            "./test_vectors/KAT_AES/CBCKeySbox192.rsp",
            "./test_vectors/KAT_AES/CBCVarKey192.rsp",
            "./test_vectors/KAT_AES/CBCVarTxt192.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes192_cbc_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _p_ut,
                    _debug_results_ut,
                ) = aes192_cbc_decrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes192_cbc_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _c_ut,
                    _debug_results_ut,
                ) = aes192_cbc_encrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")


class TestAes256CbcVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/CBCMMT256.rsp",
            "./test_vectors/KAT_AES/CBCGFSbox256.rsp",
            "./test_vectors/KAT_AES/CBCKeySbox256.rsp",
            "./test_vectors/KAT_AES/CBCVarKey256.rsp",
            "./test_vectors/KAT_AES/CBCVarTxt256.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes256_cbc_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _p_ut,
                    _debug_results_ut,
                ) = aes256_cbc_decrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes256_cbc_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _c_ut,
                    _debug_results_ut,
                ) = aes256_cbc_encrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")

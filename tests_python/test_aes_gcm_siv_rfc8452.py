"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2023

Ref: https://datatracker.ietf.org/doc/html/rfc8452
"""

# Standard imports.
import binascii
import json
import unittest


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_gcm_siv_rfc8452 import (
    gf_dot,
    gf_product,
    gf_sum,
    polyval,
    derive_keys,
)
from explore_aes_python.aes_ecb import (
    aes128_ecb_encrypt,
    aes256_ecb_encrypt,
)
from explore_aes_python.aes_gcm_siv_rfc8452 import (
    aes128_ctr_decrypt,
    aes128_ctr_encrypt,
    aes256_ctr_decrypt,
    aes256_ctr_encrypt,
    aes_gcm_siv_decrypt,
    aes_gcm_siv_encrypt,
)


class TestGfFunctions(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        with open(
            "./test_vectors/AES_GCM_SIV_RFC8452_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_fp:
            self.test_vectors = json.load(json_fp)

    def test_gf_sum(self):
        """A unit test function."""
        for i in range(0, len(self.test_vectors["AES_GCM_SIV"]["gf_sum"])):
            a_hex = self.test_vectors["AES_GCM_SIV"]["gf_sum"][i]["a"]
            a_bytes = binascii.a2b_hex(a_hex)
            a_int = int.from_bytes(
                a_bytes,
                byteorder="little",
                signed=False,
            )

            b_hex = self.test_vectors["AES_GCM_SIV"]["gf_sum"][i]["b"]
            b_bytes = binascii.a2b_hex(b_hex)
            b_int = int.from_bytes(
                b_bytes,
                byteorder="little",
                signed=False,
            )

            check_hex = self.test_vectors["AES_GCM_SIV"]["gf_sum"][i]["a + b"]
            check_bytes = binascii.a2b_hex(check_hex)
            check_int = int.from_bytes(
                check_bytes,
                byteorder="little",
                signed=False,
            )

            result_int = gf_sum(
                a_int,
                b_int,
            )

            self.assertEqual(check_int, result_int)

    def test_gf_prod(self):
        """A unit test function."""
        for i in range(0, len(self.test_vectors["AES_GCM_SIV"]["gf_product"])):
            a_hex = self.test_vectors["AES_GCM_SIV"]["gf_product"][i]["a"]
            a_bytes = binascii.a2b_hex(a_hex)
            a_int = int.from_bytes(
                a_bytes,
                byteorder="little",
                signed=False,
            )

            b_hex = self.test_vectors["AES_GCM_SIV"]["gf_product"][i]["b"]
            b_bytes = binascii.a2b_hex(b_hex)
            b_int = int.from_bytes(
                b_bytes,
                byteorder="little",
                signed=False,
            )

            check_hex = self.test_vectors["AES_GCM_SIV"]["gf_product"][i]["a * b"]
            check_bytes = binascii.a2b_hex(check_hex)
            check_int = int.from_bytes(
                check_bytes,
                byteorder="little",
                signed=False,
            )

            result_int = gf_product(
                a_int,
                b_int,
            )

            self.assertEqual(check_int, result_int)

    def test_gf_dot(self):
        """A unit test function."""
        for i in range(0, len(self.test_vectors["AES_GCM_SIV"]["gf_dot"])):
            a_hex = self.test_vectors["AES_GCM_SIV"]["gf_dot"][i]["a"]
            a_bytes = binascii.a2b_hex(a_hex)
            a_int = int.from_bytes(
                a_bytes,
                byteorder="little",
                signed=False,
            )

            b_hex = self.test_vectors["AES_GCM_SIV"]["gf_dot"][i]["b"]
            b_bytes = binascii.a2b_hex(b_hex)
            b_int = int.from_bytes(
                b_bytes,
                byteorder="little",
                signed=False,
            )

            check_hex = self.test_vectors["AES_GCM_SIV"]["gf_dot"][i]["a dot b"]
            check_bytes = binascii.a2b_hex(check_hex)
            check_int = int.from_bytes(
                check_bytes,
                byteorder="little",
                signed=False,
            )

            result_int = gf_dot(
                a_int,
                b_int,
            )

            self.assertEqual(check_int, result_int)

    def test_polyval(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.1. AEAD_AES_128_GCM_SIV"])):
            _mak_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_hex", _mak_hex)
            _mak_bytes = bytes.fromhex(_mak_hex)

            _message_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["POLYVAL input ="]
            print("_message_hex", _message_hex)
            _message_bytes = bytes.fromhex(_message_hex)

            _s_s_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["POLYVAL result ="]
            print("_s_s_check_hex", _s_s_check_hex)
            _s_s_check_bytes = bytes.fromhex(_s_s_check_hex)

            (
                _s_s_ut_bytes,
                _polyval_debug_values,
            ) = polyval(
                _mak_bytes,
                _message_bytes,
            )
            print("_s_s_ut_hex", _s_s_ut_bytes.hex())

            self.assertEqual(_s_s_ut_bytes.hex(), _s_s_check_bytes.hex())


class TestSivFunctions(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        with open("./test_vectors/AES_GCM_SIV_RFC8452_test_vectors.json", "r", encoding="utf-8") as json_fp:
            self.test_vectors = json.load(json_fp)

    def test_derive_keys_128(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.1. AEAD_AES_128_GCM_SIV"])):
            _kgk_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Key ="]
            print("_kgk_hex", _kgk_hex)
            _kgk_bytes = bytes.fromhex(_kgk_hex)

            _number_used_once_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Nonce ="]
            print("_number_used_once_hex", _number_used_once_hex)
            _number_used_once_bytes = bytes.fromhex(_number_used_once_hex)

            _mak_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_check_hex", _mak_check_hex)
            _mak_check_bytes = bytes.fromhex(_mak_check_hex)

            _mek_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record encryption key ="]
            print("_mek_check_hex", _mek_check_hex)
            _mek_check_bytes = bytes.fromhex(_mek_check_hex)

            (
                _mak_ut_bytes,
                _mek_ut_bytes,
                _,
            ) = derive_keys(
                _kgk_bytes,
                _number_used_once_bytes,
                aes128_ecb_encrypt,
            )

            self.assertEqual(_mak_ut_bytes.hex(), _mak_check_bytes.hex())
            self.assertEqual(_mek_ut_bytes.hex(), _mek_check_bytes.hex())

    def test_derive_keys_256(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.2. AEAD_AES_256_GCM_SIV"])):
            _kgk_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Key ="]
            print("_kgk_hex", _kgk_hex)
            _kgk_bytes = bytes.fromhex(_kgk_hex)

            _number_used_once_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Nonce ="]
            print("_number_used_once_hex", _number_used_once_hex)
            _number_used_once_bytes = bytes.fromhex(_number_used_once_hex)

            _mak_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_check_hex", _mak_check_hex)
            _mak_check_bytes = bytes.fromhex(_mak_check_hex)

            _mek_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Record encryption key ="]
            print("_mek_check_hex", _mek_check_hex)
            _mek_check_bytes = bytes.fromhex(_mek_check_hex)

            (
                _mak_ut_bytes,
                _mek_ut_bytes,
                _,
            ) = derive_keys(
                _kgk_bytes,
                _number_used_once_bytes,
                aes256_ecb_encrypt,
            )

            self.assertEqual(_mak_ut_bytes.hex(), _mak_check_bytes.hex())
            self.assertEqual(_mek_ut_bytes.hex(), _mek_check_bytes.hex())


class TestAesGcmSivEncrypt(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        with open(
            "./test_vectors/AES_GCM_SIV_RFC8452_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_fp:
            self.test_vectors = json.load(json_fp)

    def test_aes128_gcm_siv_encrypt(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.1. AEAD_AES_128_GCM_SIV"])):
            _pt_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Plaintext ="]
            print("_pt_hex", _pt_hex)
            _pt_bytes = bytes.fromhex(_pt_hex)

            _ad_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["AAD ="]
            print("_ad_hex", _ad_hex)
            _ad_bytes = bytes.fromhex(_ad_hex)

            _kgk_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Key ="]
            print("_kgk_hex", _kgk_hex)
            _kgk_bytes = bytes.fromhex(_kgk_hex)

            _number_used_once_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Nonce ="]
            print("_number_used_once_hex", _number_used_once_hex)
            _number_used_once_bytes = bytes.fromhex(_number_used_once_hex)

            _mak_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_check_hex", _mak_check_hex)
            _mak_check_bytes = bytes.fromhex(_mak_check_hex)

            _mek_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record encryption key ="]
            print("_mek_check_hex", _mek_check_hex)
            _mek_check_bytes = bytes.fromhex(_mek_check_hex)

            _polyval_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["POLYVAL result ="]
            print("_polyval_check_hex", _polyval_check_hex)
            _polyval_check_bytes = bytes.fromhex(_polyval_check_hex)

            _tag_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Tag ="]
            print("_tag_check_hex", _tag_check_hex)
            _tag_check_bytes = bytes.fromhex(_tag_check_hex)

            _t_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Initial counter ="]
            print("_t_check_hex", _t_check_hex)
            _t_check_bytes = bytes.fromhex(_t_check_hex)

            _result_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Result ="]
            print("_result_check_hex", _result_check_hex)
            _result_check_bytes = bytes.fromhex(_result_check_hex)

            (
                _ct_ut_bytes,
                _debug,
            ) = aes_gcm_siv_encrypt(
                key_generating_key=_kgk_bytes,
                number_used_once=_number_used_once_bytes,
                plaintext=_pt_bytes,
                additional_data=_ad_bytes,
                aes_ecb_function=aes128_ecb_encrypt,
                aes_ctr_function=aes128_ctr_encrypt,
            )

            _result_ut_bytes = _ct_ut_bytes + _debug["tag"]

            self.assertEqual(_debug["mak"].hex(), _mak_check_bytes.hex())
            self.assertEqual(_debug["mek"].hex(), _mek_check_bytes.hex())
            _polyval_ut_bytes = _debug["polyval_debug_values"][-1]
            self.assertEqual(_polyval_ut_bytes.hex(), _polyval_check_bytes.hex())
            self.assertEqual(_debug["tag"].hex(), _tag_check_bytes.hex())
            _t_ut_bytes = _debug["aes_ctr_debug_values"]["initial_ctr_block"]
            self.assertEqual(_t_ut_bytes.hex(), _t_check_bytes.hex())
            self.assertEqual(_result_ut_bytes.hex(), _result_check_bytes.hex())

    def test_aes256_gcm_siv_encrypt(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.2. AEAD_AES_256_GCM_SIV"])):
            _pt_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Plaintext ="]
            print("_pt_hex", _pt_hex)
            _pt_bytes = bytes.fromhex(_pt_hex)

            _ad_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["AAD ="]
            print("_ad_hex", _ad_hex)
            _ad_bytes = bytes.fromhex(_ad_hex)

            _kgk_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Key ="]
            print("_kgk_hex", _kgk_hex)
            _kgk_bytes = bytes.fromhex(_kgk_hex)

            _number_used_once_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Nonce ="]
            print("_number_used_once_hex", _number_used_once_hex)
            _number_used_once_bytes = bytes.fromhex(_number_used_once_hex)

            _mak_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_check_hex", _mak_check_hex)
            _mak_check_bytes = bytes.fromhex(_mak_check_hex)

            _mek_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Record encryption key ="]
            print("_mek_check_hex", _mek_check_hex)
            _mek_check_bytes = bytes.fromhex(_mek_check_hex)

            _polyval_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["POLYVAL result ="]
            print("_polyval_check_hex", _polyval_check_hex)
            _polyval_check_bytes = bytes.fromhex(_polyval_check_hex)

            _tag_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Tag ="]
            print("_tag_check_hex", _tag_check_hex)
            _tag_check_bytes = bytes.fromhex(_tag_check_hex)

            _t_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Initial counter ="]
            print("_t_check_hex", _t_check_hex)
            _t_check_bytes = bytes.fromhex(_t_check_hex)

            _result_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Result ="]
            print("_result_check_hex", _result_check_hex)
            _result_check_bytes = bytes.fromhex(_result_check_hex)

            (
                _ct_ut_bytes,
                _debug,
            ) = aes_gcm_siv_encrypt(
                key_generating_key=_kgk_bytes,
                number_used_once=_number_used_once_bytes,
                plaintext=_pt_bytes,
                additional_data=_ad_bytes,
                aes_ecb_function=aes256_ecb_encrypt,
                aes_ctr_function=aes256_ctr_encrypt,
            )

            _result_ut_bytes = _ct_ut_bytes + _debug["tag"]

            self.assertEqual(_debug["mak"].hex(), _mak_check_bytes.hex())
            self.assertEqual(_debug["mek"].hex(), _mek_check_bytes.hex())
            _polyval_ut_bytes = _debug["polyval_debug_values"][-1]
            self.assertEqual(_polyval_ut_bytes.hex(), _polyval_check_bytes.hex())
            self.assertEqual(_debug["tag"].hex(), _tag_check_bytes.hex())
            _t_ut_bytes = _debug["aes_ctr_debug_values"]["initial_ctr_block"]
            self.assertEqual(_t_ut_bytes.hex(), _t_check_bytes.hex())
            self.assertEqual(_result_ut_bytes.hex(), _result_check_bytes.hex())

    def test_aes128_gcm_siv_decrypt(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.1. AEAD_AES_128_GCM_SIV"])):
            _ct_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Result ="]
            print("_ct_hex", _ct_hex)
            _ct_bytes = bytes.fromhex(_ct_hex)

            _ad_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["AAD ="]
            print("_ad_hex", _ad_hex)
            _ad_bytes = bytes.fromhex(_ad_hex)

            _kgk_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Key ="]
            print("_kgk_hex", _kgk_hex)
            _kgk_bytes = bytes.fromhex(_kgk_hex)

            _number_used_once_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Nonce ="]
            print("_number_used_once_hex", _number_used_once_hex)
            _number_used_once_bytes = bytes.fromhex(_number_used_once_hex)

            _mak_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_check_hex", _mak_check_hex)
            _mak_check_bytes = bytes.fromhex(_mak_check_hex)

            _mek_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Record encryption key ="]
            print("_mek_check_hex", _mek_check_hex)
            _mek_check_bytes = bytes.fromhex(_mek_check_hex)

            _t_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Initial counter ="]
            print("_t_check_hex", _t_check_hex)
            _t_check_bytes = bytes.fromhex(_t_check_hex)

            _polyval_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["POLYVAL result ="]
            print("_polyval_check_hex", _polyval_check_hex)
            _polyval_check_bytes = bytes.fromhex(_polyval_check_hex)

            _pt_check_hex = _vector_set["C.1. AEAD_AES_128_GCM_SIV"][_i]["Plaintext ="]
            print("_pt_check_hex", _pt_check_hex)
            _pt_check_bytes = bytes.fromhex(_pt_check_hex)

            (
                _pt_ut_bytes,
                _debug,
            ) = aes_gcm_siv_decrypt(
                key_generating_key=_kgk_bytes,
                number_used_once=_number_used_once_bytes,
                ciphertext=_ct_bytes,
                additional_data=_ad_bytes,
                aes_ecb_function=aes128_ecb_encrypt,
                aes_ctr_function=aes128_ctr_decrypt,
            )

            self.assertEqual(_debug["mak"].hex(), _mak_check_bytes.hex())
            self.assertEqual(_debug["mek"].hex(), _mek_check_bytes.hex())
            _polyval_ut_bytes = _debug["polyval_debug_values"][-1]
            self.assertEqual(_polyval_ut_bytes.hex(), _polyval_check_bytes.hex())
            _t_ut_bytes = _debug["aes_ctr_debug_values"]["initial_ctr_block"]
            self.assertEqual(_t_ut_bytes.hex(), _t_check_bytes.hex())
            self.assertEqual(_pt_ut_bytes.hex(), _pt_check_bytes.hex())

    def test_aes256_gcm_siv_decrypt(self):
        """A unit test function."""
        _vector_set = self.test_vectors["AES_GCM_SIV"]["Appendix C. Test Vectors"]

        for _i in range(0, len(_vector_set["C.2. AEAD_AES_256_GCM_SIV"])):
            _ct_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Result ="]
            print("_ct_hex", _ct_hex)
            _ct_bytes = bytes.fromhex(_ct_hex)

            _ad_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["AAD ="]
            print("_ad_hex", _ad_hex)
            _ad_bytes = bytes.fromhex(_ad_hex)

            _kgk_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Key ="]
            print("_kgk_hex", _kgk_hex)
            _kgk_bytes = bytes.fromhex(_kgk_hex)

            _number_used_once_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Nonce ="]
            print("_number_used_once_hex", _number_used_once_hex)
            _number_used_once_bytes = bytes.fromhex(_number_used_once_hex)

            _mak_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Record authentication key ="]
            print("_mak_check_hex", _mak_check_hex)
            _mak_check_bytes = bytes.fromhex(_mak_check_hex)

            _mek_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Record encryption key ="]
            print("_mek_check_hex", _mek_check_hex)
            _mek_check_bytes = bytes.fromhex(_mek_check_hex)

            _polyval_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["POLYVAL result ="]
            print("_polyval_check_hex", _polyval_check_hex)
            _polyval_check_bytes = bytes.fromhex(_polyval_check_hex)

            _tag_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Tag ="]
            print("_tag_check_hex", _tag_check_hex)
            _tag_check_bytes = bytes.fromhex(_tag_check_hex)

            _t_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Initial counter ="]
            print("_t_check_hex", _t_check_hex)
            _t_check_bytes = bytes.fromhex(_t_check_hex)

            _pt_check_hex = _vector_set["C.2. AEAD_AES_256_GCM_SIV"][_i]["Plaintext ="]
            print("_pt_check_hex", _pt_check_hex)
            _pt_check_bytes = bytes.fromhex(_pt_check_hex)

            (
                _pt_ut_bytes,
                _debug,
            ) = aes_gcm_siv_decrypt(
                key_generating_key=_kgk_bytes,
                number_used_once=_number_used_once_bytes,
                ciphertext=_ct_bytes,
                additional_data=_ad_bytes,
                aes_ecb_function=aes256_ecb_encrypt,
                aes_ctr_function=aes256_ctr_decrypt,
            )

            self.assertEqual(_debug["mak"].hex(), _mak_check_bytes.hex())
            self.assertEqual(_debug["mek"].hex(), _mek_check_bytes.hex())
            _polyval_ut_bytes = _debug["polyval_debug_values"][-1]
            self.assertEqual(_polyval_ut_bytes.hex(), _polyval_check_bytes.hex())
            _t_ut_bytes = _debug["aes_ctr_debug_values"]["initial_ctr_block"]
            self.assertEqual(_t_ut_bytes.hex(), _t_check_bytes.hex())
            self.assertEqual(_pt_ut_bytes.hex(), _pt_check_bytes.hex())

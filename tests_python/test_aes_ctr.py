"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38a.pdf
"""

# Standard imports.
import json
import random


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_ctr import (
    aes128_ctr_encrypt,
    aes128_ctr_decrypt,
    aes192_ctr_encrypt,
    aes192_ctr_decrypt,
    aes256_ctr_encrypt,
    aes256_ctr_decrypt,
)


class TestAes128CtrBytes:
    """A unit test class."""

    def aes_ctr_round_trip(
        self,
        aes_ecb_test_vectors: dict,
        aes_ctr_encrypt_func,
        aes_ctr_decrypt_func,
    ):
        """A unit test function."""
        print("\test_round_trip")

        _key_in = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Key"])
        _pt_in = bytes.fromhex(aes_ecb_test_vectors["Encryption"]["Plaintext"])

        print("key_in         ", len(_key_in), _key_in.hex())
        print("pt_in          ", len(_pt_in), _pt_in.hex())

        _iv = random.randint(0, 2**64 - 1)
        _t_1 = _iv << 64
        _t_1 = _t_1.to_bytes(
            16,
            byteorder="big",
            signed=False,
        )

        (
            _ct_out,
            _encrypt_debug_results,
        ) = aes_ctr_encrypt_func(
            k=_key_in,
            m_len_bytes=8,
            m_be_bool=True,
            t_1=_t_1,
            p_len_bits=len(_pt_in) * 8,
            p=_pt_in,
        )

        (
            _pt_out,
            _decrypt_debug_results,
        ) = aes_ctr_decrypt_func(
            k=_key_in,
            m_len_bytes=8,
            m_be_bool=True,
            t_1=_t_1,
            c_len_bits=len(_ct_out) * 8,
            c=_ct_out,
        )

        assert _pt_out == _pt_in

    def test_aes128_ctr(self):
        """A unit test function."""
        print("\ntest_aes128_ctr")

        with open(
            "test_vectors/aes_128_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            aes_128_ecb_test_vectors = json.load(json_data)
            aes_ecb_test_vectors = aes_128_ecb_test_vectors["ECB-AES128"]

        self.aes_ctr_round_trip(
            aes_ecb_test_vectors,
            aes128_ctr_encrypt,
            aes128_ctr_decrypt,
        )

    def test_aes192_ctr(self):
        """A unit test function."""
        print("\ntest_aes128_ctr")

        with open(
            "test_vectors/aes_192_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            aes_192_ecb_test_vectors = json.load(json_data)
            aes_ecb_test_vectors = aes_192_ecb_test_vectors["ECB-AES192"]

        self.aes_ctr_round_trip(
            aes_ecb_test_vectors,
            aes192_ctr_encrypt,
            aes192_ctr_decrypt,
        )

    def test_aes256_ctr(self):
        """A unit test function."""
        print("\ntest_aes128_ctr")

        with open(
            "test_vectors/aes_256_ecb_test_vectors.json",
            "r",
            encoding="utf-8",
        ) as json_data:
            aes_128_ecb_test_vectors = json.load(json_data)
            aes_ecb_test_vectors = aes_128_ecb_test_vectors["ECB-AES256"]

        self.aes_ctr_round_trip(
            aes_ecb_test_vectors,
            aes256_ctr_encrypt,
            aes256_ctr_decrypt,
        )

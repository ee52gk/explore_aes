"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://csrc.nist.gov/pubs/sp/800/38/f/final
Ref: https://csrc.nist.gov/projects/cryptographic-algorithm-validation-program/cavp-testing-block-cipher-modes#KW
"""

# Standard imports.
import copy
import unittest


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_kwp import (
    aes128_kwp_ad,
    aes192_kwp_ad,
    aes256_kwp_ad,
)


class TestAes128Kwp(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KWP_AE_128.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_aes128_kwp_ad(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _p_ut,
                _debug_results_ut,
            ) = aes128_kwp_ad(
                _test_vectors["K"],
                _test_vectors["C"],
            )

            self.assertEqual(_p_ut, _test_vectors["P"])
            print(_test_vectors["Count"], "Pass")


class TestAes128KwpAd(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KWP_AD_128.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "C": b"",
                "P": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])
                    _p_text = text_fp.readline()
                    if _p_text == "FAIL\n":
                        self._test_vector_set[-1]["P"] = _p_text
                    else:
                        self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])

    def test_aes128_kwp_ad(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _p_ut,
                _debug_results_ut,
            ) = aes128_kwp_ad(
                _test_vectors["K"],
                _test_vectors["C"],
            )

            if _test_vectors["P"] == "FAIL\n":
                self.assertEqual(_p_ut, None)
                print("Decrypt failed for #", _test_vectors["Count"])
            else:
                self.assertEqual(_p_ut, _test_vectors["P"])
            print(_test_vectors["Count"], "Pass")


class TestAes192Kwp(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KWP_AE_192.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_aes192_kwp_ad(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _p_ut,
                _debug_results_ut,
            ) = aes192_kwp_ad(
                _test_vectors["K"],
                _test_vectors["C"],
            )

            self.assertEqual(_p_ut, _test_vectors["P"])
            print(_test_vectors["Count"], "Pass")


class TestAes192KwpAd(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KWP_AD_192.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "C": b"",
                "P": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])
                    _p_text = text_fp.readline()
                    if _p_text == "FAIL\n":
                        self._test_vector_set[-1]["P"] = _p_text
                    else:
                        self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])

    def test_aes192_kwp_ad(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _p_ut,
                _debug_results_ut,
            ) = aes192_kwp_ad(
                _test_vectors["K"],
                _test_vectors["C"],
            )

            if _test_vectors["P"] == "FAIL\n":
                self.assertEqual(_p_ut, None)
                print("Decrypt failed for #", _test_vectors["Count"])
            else:
                self.assertEqual(_p_ut, _test_vectors["P"])
            print(_test_vectors["Count"], "Pass")


class TestAes256Kwp(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KWP_AE_256.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_aes256_kwp_ad(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _p_ut,
                _debug_results_ut,
            ) = aes256_kwp_ad(
                _test_vectors["K"],
                _test_vectors["C"],
            )

            self.assertEqual(_p_ut, _test_vectors["P"])
            print(_test_vectors["Count"], "Pass")


class TestAes256KwpAd(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KWP_AD_256.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "C": b"",
                "P": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])
                    _p_text = text_fp.readline()
                    if _p_text == "FAIL\n":
                        self._test_vector_set[-1]["P"] = _p_text
                    else:
                        self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])

    def test_aes256_kwp_ad(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _p_ut,
                _debug_results_ut,
            ) = aes256_kwp_ad(
                _test_vectors["K"],
                _test_vectors["C"],
            )

            if _test_vectors["P"] == "FAIL\n":
                self.assertEqual(_p_ut, None)
                print("Decrypt failed for #", _test_vectors["Count"])
            else:
                self.assertEqual(_p_ut, _test_vectors["P"])
            print(_test_vectors["Count"], "Pass")

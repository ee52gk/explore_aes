"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://csrc.nist.gov/pubs/sp/800/38/f/final
Ref: https://csrc.nist.gov/projects/cryptographic-algorithm-validation-program/cavp-testing-block-cipher-modes#KW
"""

# Standard imports.
import copy
import json
import unittest


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_ecb import aes128_ecb_encrypt
from explore_aes_python.aes_kw import (
    rfc3394_aes_kw,
    rfc3394_aes_kw_alt,
    aes128_kw_ae,
    aes192_kw_ae,
    aes256_kw_ae,
)


class TestRfc3394Aes128Kw(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KW_AE_128.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_rfc3394_aes128_kw(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _c_ut,
                _debug_results_ut,
            ) = rfc3394_aes_kw(
                _test_vectors["P"],
                _test_vectors["K"],
                aes128_ecb_encrypt,
            )

            self.assertEqual(_c_ut, _test_vectors["C"])
            print(_test_vectors["Count"], "Pass")

    def test_rfc3394_aes128_kw_alt(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _c_ut,
                _debug_values,
            ) = rfc3394_aes_kw_alt(
                _test_vectors["P"],
                _test_vectors["K"],
                aes128_ecb_encrypt,
            )

            self.assertEqual(_c_ut, _test_vectors["C"])
            print(_test_vectors["Count"], "Pass")


class TestRfc3394Aes128KwDetail(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/aes_128_kw_test_vectors.json",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            self._test_vector_set = json.load(text_fp)

    def test_rfc3394_aes128_kw_detail(self):
        """A unit test function."""
        (
            _c_ut,
            _debug_results_ut,
        ) = rfc3394_aes_kw_alt(
            bytes.fromhex(self._test_vector_set["KW-AES128"]["Wrap"]["Key Data"]),
            bytes.fromhex(self._test_vector_set["KW-AES128"]["Wrap"]["KEK"]),
            aes128_ecb_encrypt,
        )

        for _x in range(1, len(_debug_results_ut["A"])):
            self.assertEqual(
                _debug_results_ut["A"][_x],
                bytes.fromhex(self._test_vector_set["KW-AES128"]["Wrap"]["Step t"][_x - 1]["XorT"]["A"]),
            )
            self.assertEqual(
                _debug_results_ut["R"][_x][1],
                bytes.fromhex(self._test_vector_set["KW-AES128"]["Wrap"]["Step t"][_x - 1]["XorT"]["R1"]),
            )
            self.assertEqual(
                _debug_results_ut["R"][_x][2],
                bytes.fromhex(self._test_vector_set["KW-AES128"]["Wrap"]["Step t"][_x - 1]["XorT"]["R2"]),
            )
        self.assertEqual(
            _c_ut,
            bytes.fromhex(self._test_vector_set["KW-AES128"]["Wrap"]["Ciphertext"]),
        )


class TestNistAes128Kw(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KW_AE_128.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_aes128_kw_ae(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _c_ut,
                _debug_results_ut,
            ) = aes128_kw_ae(
                _test_vectors["K"],
                _test_vectors["P"],
            )

            self.assertEqual(_c_ut, _test_vectors["C"])
            print(_test_vectors["Count"], "Pass")


class TestNistAes192Kw(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KW_AE_192.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_aes192_kw_ae(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _c_ut,
                _debug_results_ut,
            ) = aes192_kw_ae(
                _test_vectors["K"],
                _test_vectors["P"],
            )

            self.assertEqual(_c_ut, _test_vectors["C"])
            print(_test_vectors["Count"], "Pass")


class TestNistAes256Kw(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        # Read in the appropriate set of test vector files.
        with open(
            "./test_vectors/kwtestvectors/KW_AE_256.txt",
            "rt",
            encoding="utf-8",
        ) as text_fp:
            _test_vectors = {
                "Count": 0,
                "K": b"",
                "P": b"",
                "C": b"",
            }

            self._test_vector_set = []
            _not_eof = "\n"
            while _not_eof != "":
                _not_eof = text_fp.readline()
                if _not_eof[0:7] == "COUNT =":
                    self._test_vector_set.append(copy.deepcopy(_test_vectors))
                    self._test_vector_set[-1]["Count"] = int(_not_eof[8:])
                    _k_text = text_fp.readline()
                    self._test_vector_set[-1]["K"] = bytes.fromhex(_k_text[4:-1])
                    _p_text = text_fp.readline()
                    self._test_vector_set[-1]["P"] = bytes.fromhex(_p_text[4:-1])
                    _c_text = text_fp.readline()
                    self._test_vector_set[-1]["C"] = bytes.fromhex(_c_text[4:-1])

    def test_aes256_kw_ae(self):
        """A unit test function."""
        for _test_vectors in self._test_vector_set:
            (
                _c_ut,
                _debug_results_ut,
            ) = aes256_kw_ae(
                _test_vectors["K"],
                _test_vectors["P"],
            )

            self.assertEqual(_c_ut, _test_vectors["C"])
            print(_test_vectors["Count"], "Pass")

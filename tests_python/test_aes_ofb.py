"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

# Ref: https://csrc.nist.gov/pubs/sp/800/38/a/final
# Ref: https://csrc.nist.gov/Projects/cryptographic-algorithm-validation-program/Block-Ciphers
# Ref: https://pages.nist.gov/ACVP/draft-celi-acvp-symmetric.html#name-test-types
# Ref: https://stackoverflow.com/questions/69588373/aes-cbc-decryption-validation-for-monte-carlo-tests
# Ref: https://stackoverflow.com/questions/65878005/what-is-missing-from-the-aes-validation-standard-pseudocode-for-the-monte-carlo
"""

# Standard imports.
import copy
import unittest


# 3rd party imports.
import pytest


# Submodule imports.


# Local imports.
from explore_aes_python.aes_ofb import (
    aes128_ofb_decrypt,
    aes128_ofb_encrypt,
    aes192_ofb_decrypt,
    aes192_ofb_encrypt,
    aes256_ofb_decrypt,
    aes256_ofb_encrypt,
)


def read_rsp_file(filename: str) -> tuple[list, list]:
    """Read in the appropriate set of test vector files."""
    with open(
        filename,
        "rt",
        encoding="utf-8",
    ) as text_fp:
        _test_vectors = {
            "Count": 0,
            "K": b"",
            "IV": b"",
            "P": b"",
            "C": b"",
        }

        _decrypt_test_vector_set = []
        _encrypt_test_vector_set = []
        _not_eof = "\n"
        _test_vector_set = None
        while _not_eof != "":
            _not_eof = text_fp.readline()
            if _not_eof == "[DECRYPT]\n":
                _test_vector_set = _decrypt_test_vector_set
            if _not_eof == "[ENCRYPT]\n":
                _test_vector_set = _encrypt_test_vector_set
            if _not_eof[0:7] == "COUNT =":
                _test_vector_set.append(copy.deepcopy(_test_vectors))
                _test_vector_set[-1]["Count"] = int(_not_eof[8:])
                _k_text = text_fp.readline()
                _test_vector_set[-1]["K"] = bytes.fromhex(_k_text[6:-1])
                _i_text = text_fp.readline()
                _test_vector_set[-1]["IV"] = bytes.fromhex(_i_text[5:-1])
                _check_text = text_fp.readline()
                if _check_text[0] == "P":
                    _test_vector_set[-1]["P"] = bytes.fromhex(_check_text[12:-1])
                elif _check_text[0] == "C":
                    _test_vector_set[-1]["C"] = bytes.fromhex(_check_text[13:-1])
                _check_text = text_fp.readline()
                if _check_text[0] == "P":
                    _test_vector_set[-1]["P"] = bytes.fromhex(_check_text[12:-1])
                elif _check_text[0] == "C":
                    _test_vector_set[-1]["C"] = bytes.fromhex(_check_text[13:-1])

        _return = (
            _decrypt_test_vector_set,
            _encrypt_test_vector_set,
        )

        return _return


def aes_ofb_mct_decrypt(
    test_vector_set: list,
    decrypt_func,
):
    """A unit test function."""
    for _test_vectors in test_vector_set:
        # Note that progression across the 999 iterations is not clearly defined in the
        # reference. It implies that the 2nd iteration CT value is the IV from the first
        # iteration. However it does not state what the IV should be for the 2nd iteration
        # onwards. Trial end arror, helped by some StackOverflow references, shows the
        # progression is as per OFB, later IVs are the XOR of cipher and PT.
        for _j in range(0, 1000):
            if _j == 0:
                (
                    _p_ut_j,
                    _debug_values,
                ) = decrypt_func(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )
                _p_ut_jm1 = _p_ut_j
                _o_ut_jm1 = bytes(x ^ y for x, y in zip(_p_ut_jm1, _test_vectors["C"]))
                _c_ut_jp1 = _test_vectors["IV"]
            else:
                (
                    _p_ut_j,
                    _debug_values,
                ) = decrypt_func(
                    _test_vectors["K"],
                    _o_ut_jm1,
                    _c_ut_jp1,
                )
                _o_ut_jm1 = bytes(x ^ y for x, y in zip(_p_ut_j, _c_ut_jp1))
                _c_ut_jp1 = _p_ut_jm1
                _p_ut_jm1 = _p_ut_j
            if _j < 5:
                print(_j, _p_ut_j.hex())

        print(_j, _p_ut_j.hex())
        assert _p_ut_j.hex() == _test_vectors["P"].hex()
        print(_test_vectors["Count"], "Pass")


def aes_ofb_mct_encrypt(
    test_vector_set: list,
    encrypt_func,
):
    """A unit test function."""
    for _test_vectors in test_vector_set:
        # Note that progression across the 999 iterations is not clearly defined in the
        # reference. It implies that the 2nd iteration CT value is the IV from the first
        # iteration. However it does not state what the IV should be for the 2nd iteration
        # onwards. Trial end arror, helped by some StackOverflow references, shows the
        # progression is as per OFB, later IVs are the XOR of cipher and PT.
        for _j in range(0, 1000):
            if _j == 0:
                (
                    _c_ut_j,
                    _debug_values,
                ) = encrypt_func(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )
                _c_ut_jm1 = _c_ut_j
                _o_ut_jm1 = bytes(x ^ y for x, y in zip(_c_ut_jm1, _test_vectors["P"]))
                _p_ut_jp1 = _test_vectors["IV"]
            else:
                (
                    _c_ut_j,
                    _debug_values,
                ) = encrypt_func(
                    _test_vectors["K"],
                    _o_ut_jm1,
                    _p_ut_jp1,
                )
                _o_ut_jm1 = bytes(x ^ y for x, y in zip(_c_ut_j, _p_ut_jp1))
                _p_ut_jp1 = _c_ut_jm1
                _c_ut_jm1 = _c_ut_j
            if _j < 5:
                print(_j, _c_ut_j.hex())

        print(_j, _c_ut_j.hex())
        assert _c_ut_j.hex() == _test_vectors["C"].hex()
        print(_test_vectors["Count"], "Pass")


class TestAes128OfbMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (
            self._decrypt_test_vector_set,
            self._encrypt_test_vector_set,
        ) = read_rsp_file("./test_vectors/aesmct/OFBMCT128.rsp")

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes128_ofb_decrypt(self):
        """A unit test function."""
        aes_ofb_mct_decrypt(
            self._encrypt_test_vector_set,
            aes128_ofb_decrypt,
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes128_ofb_encrypt(self):
        """A unit test function."""
        aes_ofb_mct_encrypt(
            self._decrypt_test_vector_set,
            aes128_ofb_encrypt,
        )

    def test_aes128_ofb_decrypt(self):
        """A unit test function."""
        aes_ofb_mct_decrypt(
            self._decrypt_test_vector_set,
            aes128_ofb_decrypt,
        )

    def test_aes128_ofb_encrypt(self):
        """A unit test function."""
        aes_ofb_mct_encrypt(
            self._encrypt_test_vector_set,
            aes128_ofb_encrypt,
        )


class TestAes192OfbMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (
            self._decrypt_test_vector_set,
            self._encrypt_test_vector_set,
        ) = read_rsp_file("./test_vectors/aesmct/OFBMCT192.rsp")

    @pytest.mark.xfail(strinct=True)
    def test_xfail_aes192_ofb_decrypt(self):
        """A unit test function."""
        aes_ofb_mct_decrypt(
            self._encrypt_test_vector_set,
            aes192_ofb_decrypt,
        )

    @pytest.mark.xfail(strinct=True)
    def test_xfail_aes192_ofb_encrypt(self):
        """A unit test function."""
        aes_ofb_mct_encrypt(
            self._decrypt_test_vector_set,
            aes192_ofb_encrypt,
        )

    def test_aes192_ofb_decrypt(self):
        """A unit test function."""
        aes_ofb_mct_decrypt(
            self._decrypt_test_vector_set,
            aes192_ofb_decrypt,
        )

    def test_aes192_ofb_encrypt(self):
        """A unit test function."""
        aes_ofb_mct_encrypt(
            self._encrypt_test_vector_set,
            aes192_ofb_encrypt,
        )


class TestAes256OfbMct(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        (
            self._decrypt_test_vector_set,
            self._encrypt_test_vector_set,
        ) = read_rsp_file("./test_vectors/aesmct/OFBMCT256.rsp")

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes256_ofb_decrypt(self):
        """A unit test function."""
        aes_ofb_mct_decrypt(
            self._encrypt_test_vector_set,
            aes256_ofb_decrypt,
        )

    @pytest.mark.xfail(strict=True)
    def test_xfail_aes256_ofb_encrypt(self):
        """A unit test function."""
        aes_ofb_mct_encrypt(
            self._decrypt_test_vector_set,
            aes256_ofb_encrypt,
        )

    def test_aes256_ofb_decrypt(self):
        """A unit test function."""
        aes_ofb_mct_decrypt(
            self._decrypt_test_vector_set,
            aes256_ofb_decrypt,
        )

    def test_aes256_ofb_encrypt(self):
        """A unit test function."""
        aes_ofb_mct_encrypt(
            self._encrypt_test_vector_set,
            aes256_ofb_encrypt,
        )


class TestAes128OfbVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/OFBMMT128.rsp",
            "./test_vectors/KAT_AES/OFBGFSbox128.rsp",
            "./test_vectors/KAT_AES/OFBKeySbox128.rsp",
            "./test_vectors/KAT_AES/OFBVarKey128.rsp",
            "./test_vectors/KAT_AES/OFBVarTxt128.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes128_ofb_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _p_ut,
                    _debug_results_ut,
                ) = aes128_ofb_decrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes128_ofb_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _c_ut,
                    _debug_results_ut,
                ) = aes128_ofb_encrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")


class TestAes192OfbVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/OFBMMT192.rsp",
            "./test_vectors/KAT_AES/OFBGFSbox192.rsp",
            "./test_vectors/KAT_AES/OFBKeySbox192.rsp",
            "./test_vectors/KAT_AES/OFBVarKey192.rsp",
            "./test_vectors/KAT_AES/OFBVarTxt192.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes192_ofb_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _p_ut,
                    _debug_results_ut,
                ) = aes192_ofb_decrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes192_ofb_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _c_ut,
                    _debug_results_ut,
                ) = aes192_ofb_encrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")


class TestAes256OfbVarious(unittest.TestCase):
    """A unit test class."""

    def setUp(self):
        """A unit setup function."""
        self._decrypt_test_vector_sets = []
        self._encrypt_test_vector_sets = []
        for _rsp_file in [
            "./test_vectors/aesmmt/OFBMMT256.rsp",
            "./test_vectors/KAT_AES/OFBGFSbox256.rsp",
            "./test_vectors/KAT_AES/OFBKeySbox256.rsp",
            "./test_vectors/KAT_AES/OFBVarKey256.rsp",
            "./test_vectors/KAT_AES/OFBVarTxt256.rsp",
        ]:
            (
                _decrypt_test_vector_set,
                _encrypt_test_vector_set,
            ) = read_rsp_file(_rsp_file)
            self._decrypt_test_vector_sets.append(copy.deepcopy(_decrypt_test_vector_set))
            self._encrypt_test_vector_sets.append(copy.deepcopy(_encrypt_test_vector_set))

    def test_aes256_ofb_decrypt(self):
        """A unit test function."""
        for _test_vector_set in self._decrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _p_ut,
                    _debug_results_ut,
                ) = aes256_ofb_decrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["C"],
                )

                self.assertEqual(_p_ut, _test_vectors["P"])
                print(_test_vectors["Count"], "Pass")

    def test_aes256_ofb_encrypt(self):
        """A unit test function."""
        for _test_vector_set in self._encrypt_test_vector_sets:
            for _test_vectors in _test_vector_set:
                (
                    _c_ut,
                    _debug_results_ut,
                ) = aes256_ofb_encrypt(
                    _test_vectors["K"],
                    _test_vectors["IV"],
                    _test_vectors["P"],
                )

                self.assertEqual(_c_ut, _test_vectors["C"])
                print(_test_vectors["Count"], "Pass")

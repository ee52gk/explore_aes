"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

Ref: https://csrc.nist.gov/pubs/sp/800/38/f/final
Ref: https://datatracker.ietf.org/doc/html/rfc3394
"""

# Standard imports.
import copy


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_ecb import (
    AES_128_N_K,
    AES_192_N_K,
    AES_256_N_K,
    aes128_ecb_decrypt,
    aes128_ecb_encrypt,
    aes192_ecb_decrypt,
    aes192_ecb_encrypt,
    aes256_ecb_decrypt,
    aes256_ecb_encrypt,
)


BLOCK_LEN_BYTES = 16
SEMIBLOCK_LEN_BYTES = BLOCK_LEN_BYTES // 2


def nist_800_38f_aes_w(
    k: bytes,
    aes_ecb_encrypt,
    s: bytes,
) -> tuple[bytes, dict]:
    """Base wrap function from NIST 800-38F.
        Uses different parameter name, message length, and register
        offset definitions to the RFC3394 implementations.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_encrypt (_type_): The AES-ECB block cipher function to use.
        s (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    # Length of input p counts from p[1] to p[n] (semiblocks).
    # Output c stretches from c[0] to c[n], due to prepending of the fixed field semiblock.
    _s_len_bytes = len(s)
    _n = _s_len_bytes // SEMIBLOCK_LEN_BYTES

    if _n < 3:
        raise ValueError("s is not >= 3 semiblocks.", _s_len_bytes, s.hex())
    if _s_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError("s is mod semiblock = 0 in length.", _s_len_bytes)

    if aes_ecb_encrypt == aes128_ecb_encrypt:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_encrypt, len(k))
    elif aes_ecb_encrypt == aes192_ecb_encrypt:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_encrypt, len(k))
    elif aes_ecb_encrypt == aes256_ecb_encrypt:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_encrypt, len(k))
    else:
        raise ValueError("Invalid cipher function.", aes_ecb_encrypt)

    # Calculate s now to set the size of array to store a[0] thru a[s] as debug results.
    # We can then initialise a_0 into this array.
    _s = 6 * (_n - 1)
    _a = [None] * (_s + 1)
    _a[0] = s[SEMIBLOCK_LEN_BYTES * 0 : SEMIBLOCK_LEN_BYTES * 1]

    # Size the debug results array r[t][i] so that the specification offsets are honoured.
    # This means that r[x][0] will not be used, noting that Python's range(x, y) ends at y-1.
    # We then initialise r[0][i] with P[1] thru P[n]
    _r = []
    _r_t = [None] * (_n + 1)
    for _i in range(0, _s + 1):
        _r.append(copy.deepcopy(_r_t))
    for _i in range(2, _n + 1):
        _r[0][_i] = s[SEMIBLOCK_LEN_BYTES * (_i - 1) : SEMIBLOCK_LEN_BYTES * _i]

    # Execute the function loop as a set of data rotating though a function.
    for _t in range(1, _s + 1):
        # Two semiblocks feed into the block width cipher function.
        # The block width cipher functions result is later split into semiblocks.
        # In this way an uneven numbers of semiblocks (minimum 3) are supported.
        # That is important in supporting direct wrap of AES192 keys, for example.
        (_ciph_t,) = aes_ecb_encrypt(k, _a[_t - 1] + _r[_t - 1][2])

        # The 'leftmost' semiblock, a[t], is new data from the cipher each round.
        _a[_t] = bytes(
            x ^ y
            for x, y in zip(
                _ciph_t[:SEMIBLOCK_LEN_BYTES],
                _t.to_bytes(
                    SEMIBLOCK_LEN_BYTES,
                    byteorder="big",
                    signed=False,
                ),
            )
        )

        # The remaining semiblocks are shuffled 'left'.
        for _i in range(2, _n):
            _r[_t][_i] = _r[_t - 1][_i + 1]

        # The 'rightmost' semiblock is new data from the cipher each round.
        _r[_t][_n] = bytes(_ciph_t[SEMIBLOCK_LEN_BYTES:])

    # The result is the set of registers, 'leftmost' to 'rightmost' after s rounds.
    _c = _a[_s]
    for _i in range(2, _n + 1):
        _c += _r[_s][_i]

    _debug_values = {
        "A": _a,
        "R": _r,
    }

    _return = (
        _c,
        _debug_values,
    )

    return _return


def nist_800_38f_aes_w_inv(
    k: bytes,
    aes_ecb_decrypt,
    c: bytes,
) -> tuple[bytes, dict]:
    """Base unwrap function from NIST 800-38F.
        Uses different parameter name, message length, and register
        offset definitions to the RFC3394 implementations.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_decrypt (_type_): The AES-ECB block cipher function to use.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    # Length of input p counts from p[1] to p[n] (semiblocks).
    # Output c stretches from c[0] to c[n], due to prepending of the fixed field semiblock.
    _c_len_bytes = len(c)
    _n = _c_len_bytes // SEMIBLOCK_LEN_BYTES

    if _n < 3:
        raise ValueError("c is not >= 3 semiblocks.", _c_len_bytes, c.hex())
    if _c_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError("c is mod semiblock = 0 in length.", _c_len_bytes)

    if aes_ecb_decrypt == aes128_ecb_decrypt:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_decrypt, len(k))
    elif aes_ecb_decrypt == aes192_ecb_decrypt:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_decrypt, len(k))
    elif aes_ecb_decrypt == aes256_ecb_decrypt:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_decrypt, len(k))
    else:
        raise ValueError("Invalid cipher function.", aes_ecb_decrypt)

    # Calculate s now to set the size of array to store a[0] thru a[s] as debug results.
    # We can then initialise a_0 into this array.
    _s = 6 * (_n - 1)
    _a = [None] * (_s + 1)
    _a[_s] = c[SEMIBLOCK_LEN_BYTES * 0 : SEMIBLOCK_LEN_BYTES * 1]

    # Size the debug results array r[t][i] so that the specification offsets are honoured.
    # This means that r[x][0] will not be used, noting that Python's range(x, y) ends at y-1.
    # We then initialise r[0][i] with P[1] thru P[n]
    _r = []
    _r_t = [None] * (_n + 1)
    for _i in range(0, _s + 1):
        _r.append(copy.deepcopy(_r_t))
    for _i in range(2, _n + 1):
        _r[_s][_i] = c[SEMIBLOCK_LEN_BYTES * (_i - 1) : SEMIBLOCK_LEN_BYTES * _i]

    # Execute the function loop as a set of data rotating though a function.
    for _t in range(_s, 0, -1):
        # The 'leftmost' semiblock, a[t], is new data from the cipher each round.
        _a_xor_t = bytes(
            x ^ y
            for x, y in zip(
                _a[_t][:SEMIBLOCK_LEN_BYTES], _t.to_bytes(SEMIBLOCK_LEN_BYTES, byteorder="big", signed=False)
            )
        )

        # Two semiblocks feed into the block width cipher function.
        # The block width cipher functions result is later split into semiblocks.
        # In this way an uneven numbers of semiblocks (minimum 3) are supported.
        # That is important in supporting direct wrap of AES192 keys, for example.
        (_ciph_t,) = aes_ecb_decrypt(
            k,
            _a_xor_t + _r[_t][_n],
        )

        # The 'leftmost' semiblock is new data from the cipher each round.
        _a[_t - 1] = bytes(_ciph_t[:SEMIBLOCK_LEN_BYTES])

        # The next 'leftmost' semiblock is new data from the cipher each round.
        _r[_t - 1][2] = bytes(_ciph_t[SEMIBLOCK_LEN_BYTES:])

        # The remaining semiblocks are shuffled 'right'.
        for _i in range(2, _n):
            _r[_t - 1][_i + 1] = _r[_t][_i]

    # The result is the set of registers, 'leftmost' to 'rightmost' after s rounds.
    _s = _a[0]
    for _i in range(2, _n + 1):
        _s += _r[0][_i]

    _debug_values = {
        "A": _a,
        "R": _r,
    }

    _return = (
        _s,
        _debug_values,
    )

    return _return


def rfc3394_aes_kw(
    p: bytes,
    k: bytes,
    aes_ecb_encrypt,
) -> tuple[bytes, dict]:
    """Wrap function from RFC 3394, section 2.2.1.
        Uses different parameter name, message length, and register
        offset definitions to the NIST implementations.

    Args:
        p (bytes): The plaintext.
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_encrypt (_type_): The AES-ECB block cipher function to use.

    Raises:
        ValueError: p is an invalid length; not an integer multiple of a semiblock length.
        ValueError: p is not at least 3 semiblocks in length.
        ValueError: Various key length vs cipher function conditions.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    # Length of input p counts from p[1] to p[n] (semiblocks).
    # Output c stretches from c[0] to c[n], due to prepending of the fixed field semiblock.
    _p_len_bytes = len(p)
    _n = _p_len_bytes // SEMIBLOCK_LEN_BYTES

    if _n < 2:
        raise ValueError("p is not >= 2 semiblocks.", _p_len_bytes, p.hex())
    if _p_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError("p is mod semiblock = 0 in length.", _p_len_bytes)

    if aes_ecb_encrypt == aes128_ecb_encrypt:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_encrypt, len(k))
    elif aes_ecb_encrypt == aes192_ecb_encrypt:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_encrypt, len(k))
    elif aes_ecb_encrypt == aes256_ecb_encrypt:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_encrypt, len(k))
    else:
        raise ValueError("Invalid cipher function.", aes_ecb_encrypt)

    # Calculate s now to set the size of array to store a[0] thru a[s] as debug results.
    # We can then initialise a_0 into this array.
    _s = 6 * _n
    _a = [None] * (_s + 1)
    _a[0] = bytes.fromhex("A6A6A6A6A6A6A6A6")

    # Size the debug results array r[t][i] so that the specification offsets are honoured.
    # This means that r[x][0] will not be used, noting that Python's range(x, y) ends at y-1.
    # We then initialise r[0][i] with P[1] thru P[n]
    _r = []
    _r_t = [None] * (_n + 1)
    for _i in range(0, _s + 1):
        _r.append(copy.deepcopy(_r_t))
    for _i in range(1, _n + 1):
        _r[0][_i] = p[SEMIBLOCK_LEN_BYTES * (_i - 1) : SEMIBLOCK_LEN_BYTES * _i]

    # Execute the function loop as a set of data rotating though a function.
    for _t in range(1, _s + 1):
        # Two semiblocks feed into the block width cipher function.
        # The block width cipher functions result is later split into semiblocks.
        # In this way an uneven numbers of semiblocks (minimum 3) are supported.
        # That is important in supporting direct wrap of AES192 keys, for example.
        (_ciph_t,) = aes_ecb_encrypt(k, _a[_t - 1] + _r[_t - 1][1])

        # The 'leftmost' semiblock, a[t], is new data from the cipher each round.
        _a[_t] = bytes(
            x ^ y
            for x, y in zip(
                _ciph_t[:SEMIBLOCK_LEN_BYTES],
                _t.to_bytes(
                    SEMIBLOCK_LEN_BYTES,
                    byteorder="big",
                    signed=False,
                ),
            )
        )

        # The remaining semiblocks are shuffled 'left'.
        for _i in range(1, _n):
            _r[_t][_i] = _r[_t - 1][_i + 1]

        # The 'rightmost' semiblock is new data from the cipher each round.
        _r[_t][_n] = bytes(_ciph_t[SEMIBLOCK_LEN_BYTES:])

    # The result is the set of registers, 'leftmost' to 'rightmost' after s rounds.
    _c = _a[_s]
    for _i in range(1, _n + 1):
        _c += _r[_s][_i]

    _debug_values = {
        "A": _a,
        "R": _r,
    }

    _return = (
        _c,
        _debug_values,
    )

    return _return


def rfc3394_aes_kw_alt(
    p: bytes,
    k: bytes,
    aes_ecb_encrypt,
) -> tuple[bytes, dict]:
    """Key Wrap function alternate as defined in RFC 3394.
        Using in place locations.
        This construction is used to create the RFC 3394 step by step test vectors.
        To match this construction this function storessdebug value arrays separately.

    Args:
        p (bytes): The plaintext.
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_encrypt (_type_): The AES-ECB block cipher function to use.

    Raises:
        ValueError: p is an invalid length; not an integer multiple of a semiblock length.
        ValueError: p is not at least 3 semiblocks in length.
        ValueError: Various key length vs cipher function conditions.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    # Length of input p counts from p[1] to p[n] (semiblocks).
    # Output c stretches from c[0] to c[n], due to prepending of the fixed field semiblock.
    _p_len_bytes = len(p)
    _n = _p_len_bytes // SEMIBLOCK_LEN_BYTES

    if _n < 2:
        raise ValueError("p is not at least 2 semiblocks.", _p_len_bytes, p.hex())
    if _p_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError("p is not multiple of a semiblock in length.", _p_len_bytes)

    if aes_ecb_encrypt == aes128_ecb_encrypt:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Mismatched cipher function and key length.", aes_ecb_encrypt, len(k))
    elif aes_ecb_encrypt == aes192_ecb_encrypt:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Mismatched cipher function and key length.", aes_ecb_encrypt, len(k))
    elif aes_ecb_encrypt == aes256_ecb_encrypt:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Mismatched cipher function and key length.", aes_ecb_encrypt, len(k))
    else:
        raise ValueError("Invalid cipher function.", aes_ecb_encrypt)

    # Initialise a.
    _a = bytes.fromhex("A6A6A6A6A6A6A6A6")
    _debug_values = {
        "A": [None],
        "R": [],
    }
    _debug_values["A"][0] = _a

    # Initialise r[i] with P[1] thru P[n], note r[0] is unused.
    _r = [None]
    _r_i = [None]
    _debug_values["R"].append(copy.deepcopy(_r_i))
    for _i in range(1, _n + 1):
        _r.append(p[SEMIBLOCK_LEN_BYTES * (_i - 1) : SEMIBLOCK_LEN_BYTES * _i])
        _debug_values["R"][0].append(_r[_i])

    # Execute the function loop as a set of data rotating though a function.
    for _j in range(0, 6):
        for _i in range(1, _n + 1):
            _debug_values["R"].append(copy.deepcopy(_r_i))
            # Two semiblocks feed into the block width cipher function.
            # The block width cipher functions result is later split into semiblocks.
            # In this way an uneven numbers of semiblocks (minimum 3) are supported.
            # That is important in supporting direct wrap of AES192 keys, for example.
            (_b,) = aes_ecb_encrypt(k, _a + _r[_i])

            _t = (_n * _j) + _i
            _a = bytes(
                x ^ y
                for x, y in zip(
                    _b[:SEMIBLOCK_LEN_BYTES],
                    _t.to_bytes(
                        SEMIBLOCK_LEN_BYTES,
                        byteorder="big",
                        signed=False,
                    ),
                )
            )
            _debug_values["A"].append(copy.deepcopy(_a))

            _r[_i] = bytes(_b[SEMIBLOCK_LEN_BYTES:])
            for _dv_i in range(1, _n + 1):
                _debug_values["R"][_t].append(copy.deepcopy(_r[_dv_i]))

    # The result is the set of registers, 'leftmost' to 'rightmost' after s rounds.
    _c = _a
    for _i in range(1, _n + 1):
        _c += _r[_i]

    _return = (
        _c,
        _debug_values,
    )

    return _return


def rfc3394_aes_ku(
    c: bytes,
    k: bytes,
    aes_ecb_decrypt,
) -> tuple[bytes, dict]:
    """Unwrap function from RFC 3394, section 2.2.1.
        Uses different parameter name, message length, and register
        offset definitions to the NIST implementations.

    Args:
        c (bytes): The ciphertext.
        k (bytes): The cipher key, must match aes_ecb_decrypt, e.g. 128-bits for AES128.
        aes_ecb_decrypt (_type_): The AES-ECB block cipher function to use.

    Raises:
        ValueError: c is an invalid length; not an integer multiple of a semiblock length.
        ValueError: c is not at least 3 semiblocks in length.
        ValueError: Various key length vs cipher function conditions.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    # Length of input p counts from p[1] to p[n] (semiblocks).
    # Output c stretches from c[0] to c[n], due to prepending of the fixed field semiblock.
    _c_len_bytes = len(c)
    _n = (_c_len_bytes // SEMIBLOCK_LEN_BYTES) - 1

    if _n < 2:
        raise ValueError("c is not >= 2 semiblocks.", _c_len_bytes, c.hex())
    if _c_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError("c is mod semiblock = 0 in length.", _c_len_bytes)

    if aes_ecb_decrypt == aes128_ecb_decrypt:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_decrypt, len(k))
    elif aes_ecb_decrypt == aes192_ecb_decrypt:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_decrypt, len(k))
    elif aes_ecb_decrypt == aes256_ecb_decrypt:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Mismatched cipher function & key length.", aes_ecb_decrypt, len(k))
    else:
        raise ValueError("Invalid cipher function.", aes_ecb_decrypt)

    # Calculate s now to set the size of array to store a[0] thru a[s] as debug results.
    # We can then initialise a_0 into this array.
    _s = 6 * _n
    _a = [None] * (_s + 1)
    _a[_s] = c[SEMIBLOCK_LEN_BYTES * 0 : SEMIBLOCK_LEN_BYTES * 1]

    # Size the debug results array r[t][i] so that the specification offsets are honoured.
    # This means that r[x][0] will not be used, noting that Python's range(x, y) ends at y-1.
    # We then initialise r[0][i] with P[1] thru P[n]
    _r = []
    _r_t = [None] * (_n + 1)
    for _i in range(0, _s + 1):
        _r.append(copy.deepcopy(_r_t))
    for _i in range(1, _n + 1):
        _r[_s][_i] = c[SEMIBLOCK_LEN_BYTES * _i : SEMIBLOCK_LEN_BYTES * (_i + 1)]

    # Execute the function loop as a set of data rotating though a function.
    for _t in range(_s, 0, -1):
        _a_xor_t = bytes(
            x ^ y
            for x, y in zip(
                _a[_t][:SEMIBLOCK_LEN_BYTES],
                _t.to_bytes(
                    SEMIBLOCK_LEN_BYTES,
                    byteorder="big",
                    signed=False,
                ),
            )
        )

        # Two semiblocks feed into the block width cipher function.
        # The block width cipher functions result is later split into semiblocks.
        # In this way an uneven numbers of semiblocks (minimum 3) are supported.
        # That is important in supporting direct wrap of AES192 keys, for example.
        (_ciph_t,) = aes_ecb_decrypt(k, _a_xor_t + _r[_t][_n])

        # The 'leftmost' semiblock, a[t-1], is new data from the cipher each round.
        _a[_t - 1] = _ciph_t[:SEMIBLOCK_LEN_BYTES]

        # The 'rightmost' semiblock is new data from the cipher each round.
        _r[_t - 1][1] = bytes(_ciph_t[SEMIBLOCK_LEN_BYTES:])

        # The remaining semiblocks are shuffled 'right'.
        for _i in range(2, _n + 1):
            _r[_t - 1][_i] = _r[_t][_i - 1]

    # The result is the set of registers, 'leftmost' to 'rightmost' after s rounds.
    if _a[0] == bytes.fromhex("a6a6a6a6a6a6a6a6"):
        _p = b""
        for _i in range(1, _n + 1):
            _p += _r[0][_i]
    else:
        _p = None

    _debug_values = {
        "A": _a,
        "R": _r,
    }

    _return = (
        _p,
        _debug_values,
    )

    return _return


def rfc3394_aes_ku_alt(
    c: bytes,
    k: bytes,
    aes_ecb_decrypt,
) -> tuple[bytes, dict]:
    """Key Unwrap function alternate as defined in RFC 3394.
        Using in place locations.
        This construction is used to create the RFC 3394 step by step test vectors.
        To match this construction this function storessdebug value arrays separately.

    Args:
        c (bytes): The ciphertext.
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_decrypt (_type_): The AES-ECB block cipher function to use.

    Raises:
        ValueError: c is an invalid length; not an integer multiple of a semiblock length.
        ValueError: c is not at least 3 semiblocks in length.
        ValueError: Various key length vs cipher function conditions.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    # Length of input c counts from c[0] to c[n] (semiblocks).
    # Output c stretches from c[1] to p[n], due to removal of the fixed field semiblock.
    _c_len_bytes = len(c)
    _n = _c_len_bytes // SEMIBLOCK_LEN_BYTES - 1

    if _n < 2:
        raise ValueError("c is not at least 3 semiblocks.", _c_len_bytes, c.hex())
    if _c_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError("c is not multiple of a semiblock in length.", _c_len_bytes)

    if aes_ecb_decrypt == aes128_ecb_decrypt:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Mismatched cipher function and key length.", aes_ecb_decrypt, len(k))
    elif aes_ecb_decrypt == aes192_ecb_decrypt:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Mismatched cipher function and key length.", aes_ecb_decrypt, len(k))
    elif aes_ecb_decrypt == aes256_ecb_decrypt:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Mismatched cipher function and key length.", aes_ecb_decrypt, len(k))
    else:
        raise ValueError("Invalid cipher function.", aes_ecb_decrypt)

    # Initialise a.
    _a = c[SEMIBLOCK_LEN_BYTES * 0 : SEMIBLOCK_LEN_BYTES * 1]
    _debug_values = {
        "A": [None],
        "R": [],
    }
    _debug_values["A"][0] = _a

    # We initialise r[i] with P[1] thru P[n], note r[0] is unused.
    _r = [None]
    _r_i = [None]
    _debug_values["R"].append(copy.deepcopy(_r_i))
    for _i in range(1, _n + 1):
        _r.append(c[SEMIBLOCK_LEN_BYTES * _i : SEMIBLOCK_LEN_BYTES * (_i + 1)])
        _debug_values["R"][0].append(_r[_i])

    # Execute the function loop as a set of data rotating though a function.
    for _j in range(5, -1, -1):
        for _i in range(_n, 0, -1):
            _debug_values["R"].append(copy.deepcopy(_r_i))
            # Two semiblocks feed into the block width cipher function.
            # The block width cipher functions result is later split into semiblocks.
            # In this way an uneven numbers of semiblocks (minimum 3) are supported.
            # That is important in supporting direct wrap of AES192 keys, for example.
            _t = (_n * _j) + _i
            _a_xor_t = bytes(
                x ^ y
                for x, y in zip(
                    _a,
                    _t.to_bytes(
                        SEMIBLOCK_LEN_BYTES,
                        byteorder="big",
                        signed=False,
                    ),
                )
            )
            (_b,) = aes_ecb_decrypt(k, _a_xor_t + _r[_i])

            _a = _b[:SEMIBLOCK_LEN_BYTES]
            _debug_values["A"].append(copy.deepcopy(_a))

            _r[_i] = bytes(_b[SEMIBLOCK_LEN_BYTES:])
            for _dv_i in range(1, _n + 1):
                _debug_values["R"][-1].append(copy.deepcopy(_r[_dv_i]))

    # The result is the set of registers, 'leftmost' to 'rightmost' after s rounds.
    if _a == bytes.fromhex("A6A6A6A6A6A6A6A6"):
        _p = b""
        for _i in range(1, _n + 1):
            _p += _r[_i]
    else:
        _p = None

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes_kw_ae(
    k: bytes,
    aes_ecb_encrypt,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_encrypt (_type_): The AES-ECB block cipher function to use.
        p (bytes): The plaintext.

    Raises:
        ValueError: p is an invalid length; not an integer multiple of a semiblock length.
        ValueError: p is not at least 3 semiblocks in length.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    _p_len_bytes = len(p)
    if _p_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError(
            "P is not an integer multiple of the semiblock length.", _p_len_bytes, _p_len_bytes % SEMIBLOCK_LEN_BYTES
        )
    if _p_len_bytes // SEMIBLOCK_LEN_BYTES < 2:
        raise ValueError(
            "P is not at least 3 semiblocks in length.", _p_len_bytes, _p_len_bytes // SEMIBLOCK_LEN_BYTES
        )

    _icv1_hex = "A6A6A6A6A6A6A6A6"
    _icv1_bytes = bytes.fromhex(_icv1_hex)

    _s = _icv1_bytes + p

    (
        _c,
        _debug_values,
    ) = nist_800_38f_aes_w(
        k,
        aes_ecb_encrypt,
        _s,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes_kw_ad(
    k: bytes,
    aes_ecb_decrypt,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_decrypt (_type_): The AES-ECB block cipher function to use.
        c (bytes): The ciphertext.

    Raises:
        ValueError: c is an invalid length; not an integer multiple of a semiblock length.
        ValueError: c is not at least 3 semiblocks in length.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    _c_len_bytes = len(c)
    if _c_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError(
            "C is not an integer multiple of the semiblock length.", _c_len_bytes, _c_len_bytes % SEMIBLOCK_LEN_BYTES
        )
    if _c_len_bytes // SEMIBLOCK_LEN_BYTES < 2:
        raise ValueError(
            "C is not at least 3 semiblocks in length.", _c_len_bytes, _c_len_bytes // SEMIBLOCK_LEN_BYTES
        )

    _icv1_hex = "A6A6A6A6A6A6A6A6"
    _icv1_bytes = bytes.fromhex(_icv1_hex)

    (
        _s,
        _debug_values,
    ) = nist_800_38f_aes_w_inv(
        k,
        aes_ecb_decrypt,
        c,
    )

    _icv_check = _s[:SEMIBLOCK_LEN_BYTES]
    if _icv_check == _icv1_bytes:
        _p = _s[SEMIBLOCK_LEN_BYTES:]
    else:
        _p = None

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes128_kw_ae(
    k: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        p (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    (_c, _debug_values) = aes_kw_ae(
        k,
        aes128_ecb_encrypt,
        p,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes192_kw_ae(
    k: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        p (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    (_c, _debug_values) = aes_kw_ae(
        k,
        aes192_ecb_encrypt,
        p,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes256_kw_ae(
    k: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        p (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    (_c, _debug_values) = aes_kw_ae(
        k,
        aes256_ecb_encrypt,
        p,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes128_kw_ad(
    k: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    (_p, _debug_values) = aes_kw_ad(
        k,
        aes128_ecb_decrypt,
        c,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes192_kw_ad(
    k: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    (_p, _debug_values) = aes_kw_ad(
        k,
        aes192_ecb_decrypt,
        c,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes256_kw_ad(
    k: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    (_p, _debug_values) = aes_kw_ad(
        k,
        aes256_ecb_decrypt,
        c,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return

"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

Ref: https://csrc.nist.gov/pubs/sp/800/38/a/final
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_128_RCON_BYTES,
    AES_192_RCON_BYTES,
    AES_256_RCON_BYTES,
    AES_128_N_K,
    AES_192_N_K,
    AES_256_N_K,
    AES_128_N_B,
    AES_192_N_B,
    AES_256_N_B,
    AES_128_N_R,
    AES_192_N_R,
    AES_256_N_R,
)
from explore_aes_python.aes_key_expansion import (
    key_expansion_bytes,
)
from explore_aes_python.aes_functions import (
    cipher_bytes,
    inv_cipher_bytes,
)


def aes_cbc_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
    r_con: bytes,
    n_k: int,
    n_r: int,
    n_b: int = 4,
) -> tuple[bytes, dict]:
    """Wrapper function providing a parameterised AES-CBC mode encrypt."""
    if len(k) % (4 * n_k) != 0:
        raise ValueError("Key length is not 4 * 8 * n_k bits", n_k, len(k) * 8)
    if len(iv) != (4 * n_b):
        raise ValueError("CBC IV is not a block length", len(iv), iv.hex())
    if len(p) % (4 * n_b) != 0:
        raise ValueError("Plain text length is not a block length multiple", n_b, len(p))

    (
        _w,
        _,
    ) = key_expansion_bytes(
        key=k,
        r_con=r_con,
        n_k=n_k,
        n_r=n_r,
        n_b=n_b,
    )

    _c_jm1 = iv
    _c = bytearray()
    _debug_values = {"_p_j_xor_c_jm1": []}
    for _j in range(0, len(p), 4 * n_b):
        _p_j = p[_j : _j + 4 * n_b]
        _p_j_xor_c_jm1 = bytes(x ^ y for x, y in zip(_p_j, _c_jm1))
        _debug_values["_p_j_xor_c_jm1"].append(_p_j_xor_c_jm1)
        (
            _c_j,
            _,
        ) = cipher_bytes(
            block=_p_j_xor_c_jm1,
            w=_w,
            n_k=n_k,
            n_r=n_r,
            n_b=n_b,
        )
        _c += _c_j
        _c_jm1 = _c_j

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes_cbc_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
    r_con: bytes,
    n_k: int,
    n_r: int,
    n_b: int = 4,
) -> tuple[bytes, dict]:
    """Wrapper function providing a parameterised AES-CBC mode decrypt."""
    if len(k) % (4 * n_k) != 0:
        raise ValueError("Key length is not 4 * 8 * n_k bits", n_k, len(k) * 8)
    if len(iv) != (4 * n_b):
        raise ValueError("CBC IV is not a block length", len(iv), iv.hex())
    if len(c) % (4 * n_b) != 0:
        raise ValueError("Cipher text length is not a block length multiple", n_b, len(c))

    (
        _w,
        _,
    ) = key_expansion_bytes(
        key=k,
        r_con=r_con,
        n_k=n_k,
        n_r=n_r,
        n_b=n_b,
    )

    _c_jm1 = iv
    _p = bytearray()
    _debug_values = {
        "P_j_xor_C_jm1": [],
    }
    for _j in range(0, len(c), 4 * n_b):
        _c_j = c[_j : _j + 4 * n_b]
        (
            _p_j_xor_c_jm1,
            _,
        ) = inv_cipher_bytes(
            block=_c_j,
            w=_w,
            n_k=n_k,
            n_r=n_r,
            n_b=n_b,
        )
        _debug_values["P_j_xor_C_jm1"].append(_p_j_xor_c_jm1)
        _p_j = bytes(x ^ y for x, y in zip(_p_j_xor_c_jm1, _c_jm1))
        _p += _p_j
        _c_jm1 = _c_j

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes128_cbc_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES128-CBC mode encrypt."""
    (
        _c,
        _debug_values,
    ) = aes_cbc_encrypt(
        k=k,
        iv=iv,
        p=p,
        r_con=AES_128_RCON_BYTES,
        n_k=AES_128_N_K,
        n_r=AES_128_N_R,
        n_b=AES_128_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes128_cbc_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES128-CBC mode decrypt."""
    (
        _p,
        _debug_values,
    ) = aes_cbc_decrypt(
        k=k,
        iv=iv,
        c=c,
        r_con=AES_128_RCON_BYTES,
        n_k=AES_128_N_K,
        n_r=AES_128_N_R,
        n_b=AES_128_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes192_cbc_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES192-CBC mode encrypt."""
    (
        _c,
        _debug_values,
    ) = aes_cbc_encrypt(
        k=k,
        iv=iv,
        p=p,
        r_con=AES_192_RCON_BYTES,
        n_k=AES_192_N_K,
        n_r=AES_192_N_R,
        n_b=AES_192_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes192_cbc_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES192-CBC mode decrypt."""
    (
        _p,
        _debug_values,
    ) = aes_cbc_decrypt(
        k=k,
        iv=iv,
        c=c,
        r_con=AES_192_RCON_BYTES,
        n_k=AES_192_N_K,
        n_r=AES_192_N_R,
        n_b=AES_192_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes256_cbc_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES256-CBC mode encrypt."""
    (
        _c,
        _debug_values,
    ) = aes_cbc_encrypt(
        k=k,
        iv=iv,
        p=p,
        r_con=AES_256_RCON_BYTES,
        n_k=AES_256_N_K,
        n_r=AES_256_N_R,
        n_b=AES_256_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes256_cbc_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES256-CBC mode decrypt."""
    (
        _p,
        _debug_values,
    ) = aes_cbc_decrypt(
        k=k,
        iv=iv,
        c=c,
        r_con=AES_256_RCON_BYTES,
        n_k=AES_256_N_K,
        n_r=AES_256_N_R,
        n_b=AES_256_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return

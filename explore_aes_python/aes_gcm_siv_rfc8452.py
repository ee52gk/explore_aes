"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2023

Ref: https://datatracker.ietf.org/doc/html/rfc8452
"""

# Standard imports.
import binascii
import copy


# 3rd party imports.


# Submodule imports.


# Local imports.
# Note that AES-GCM SIV is not defined for AES192 block cipher (24 bit keys).
from explore_aes_python.aes_ecb import (
    aes128_ecb_encrypt,
    aes256_ecb_encrypt,
)
from explore_aes_python.aes_ctr import (
    AES_128_N_K,
    AES_256_N_K,
    aes128_ctr_decrypt,
    aes128_ctr_encrypt,
    aes256_ctr_decrypt,
    aes256_ctr_encrypt,
)

AEAD_AES_128_GCM_SIV_K_LEN = 4 * AES_128_N_K
AEAD_AES_128_GCM_SIV_P_MAX = 2**36
AEAD_AES_128_GCM_SIV_A_MAX = 2**36
AEAD_AES_128_GCM_SIV_N_MIN = 12
AEAD_AES_128_GCM_SIV_N_MAX = 12
AEAD_AES_128_GCM_SIV_C_MAX = 2**23 + 16

AEAD_AES_256_GCM_SIV_K_LEN = 4 * AES_256_N_K
AEAD_AES_128_GCM_SIV_P_MAX = 2**36
AEAD_AES_128_GCM_SIV_A_MAX = 2**36
AEAD_AES_128_GCM_SIV_N_MIN = 12
AEAD_AES_128_GCM_SIV_N_MAX = 12
AEAD_AES_128_GCM_SIV_C_MAX = 2**36 + 16

IP_bytes = binascii.a2b_hex("010000000000000000000000000000C201")
IP_int = int.from_bytes(IP_bytes, byteorder="little", signed=False)

Xttm128_bytes = binascii.a2b_hex("01000000000000000000000000000492")
Xttm128_int = int.from_bytes(Xttm128_bytes, byteorder="little", signed=False)


def gf_sum(
    a: int,
    b: int,
) -> int:
    """POLYVAL Galois field sum function.
    RFC 8452 section 3.
    """
    _return = a ^ b

    return _return


def gf_product(
    a: int,
    b: int,
) -> int:
    """POLYVAL Galois field product function.
    RFC 8452 section 3.
    """
    if a < 0 or a >= (1 << 128):
        raise ValueError("Incorrect GF value length")
    if b < 0 or b >= (1 << 128):
        raise ValueError("Incorrect GF value length")

    # sum is defined as XOR
    # So as 11001 * 10101 is usually:
    #           1 *     10101 +
    #          0  *    101010 +
    #         0   *   1010100 +
    #        1    *  10101000 +
    #       1     * 101010000
    # Now 11001 * 10101 is:
    #           1 *     10101 XOR
    #          0  *    101010 XOR
    #         0   *   1010100 XOR
    #        1    *  10101000 XOR
    #       1     * 101010000
    p = 0
    for i in range(0, 128):
        if b >> i & 0x1 == 0x1:
            p = gf_sum(p, (a << i))

    # The Galois Field is defined versus IP, so we must then take MOD IP
    _return = gf_mod(
        p,
        IP_int,
    )

    return _return


def gf_mod(
    a: int,
    m: int = Xttm128_int,
) -> int:
    """POLYVAL Galois field modulo reduction function.
    RFC 8452 section 3.
    """
    if a < 0 or a >= (1 << 256):
        raise ValueError("Incorrect GF value length")
    if m < 0 or m >= (1 << 129):
        raise ValueError("Incorrect GF value length")

    m_product = m
    leftshift = 0

    while m_product < a:
        leftshift += 1
        m_product = m << leftshift

    new_result = a
    min_result = a
    while leftshift >= 0:
        m_product = m << leftshift
        new_result = min_result ^ m_product

        if new_result < min_result:
            min_result = new_result
        leftshift -= 1

    return min_result


def gf_dot(
    a: int,
    b: int,
) -> int:
    """POLYVAL Galois field dot product function.
    RFC 8452 section 3.
    """
    if a < 0 or a >= (1 << 128):
        raise ValueError("Incorrect GF value length")
    if b < 0 or b >= (1 << 128):
        raise ValueError("Incorrect GF value length")

    temp = gf_product(
        a,
        b,
    )

    _return = gf_product(
        temp,
        Xttm128_int,
    )

    return _return


def polyval(
    h: bytes,
    message: bytes,
) -> tuple[bytearray, list]:
    """POLYVAL function.
    RFC 8452 section 3.
    """
    if len(h) != 16:
        raise ValueError("Incorrect key length")
    if len(message) % 16 != 0:
        raise ValueError("Incorrect input length")

    _h_int = int.from_bytes(
        h,
        byteorder="little",
        signed=False,
    )
    _s_int = [0x0]
    for _i in range(0, len(message) // 16):
        _x_j_bytes = message[16 * _i : 16 * (_i + 1)]
        _x_j_int = int.from_bytes(
            _x_j_bytes,
            byteorder="little",
            signed=False,
        )
        _dot_a = gf_sum(
            _s_int[_i],
            _x_j_int,
        )
        _s_int.append(
            gf_dot(
                _dot_a,
                _h_int,
            )
        )

    _s_bytes = []
    for _s in _s_int:
        _s_bytes.append(
            _s.to_bytes(
                16,
                byteorder="little",
                signed=False,
            )
        )
    _return = (
        bytearray(_s_bytes[-1]),
        _s_bytes,
    )

    return _return


def derive_keys(
    key_generating_key: bytes,
    number_used_once: bytes,
    aes_func,
) -> tuple[bytes, bytes, dict]:
    """Key generation function.
    This is an element of the GCM SIV nonce misuse resistant construction.
    Re-use of a key-nonce tuple with AES-CTR across two messages is catastrophic to security.
    Making the generated key used with AES-CTR dependent on the nonce,
    and the counter block used with AES-CTR dependent on the derive key value and the message,
    makes certain that two messages processed with AES-GCM SIV do not use the same key-nonce
    tuple with the AES-CTR mode encryption element of the algorithm.
    RFC 8452 section 4.
    """
    if len(key_generating_key) == 4 * AES_128_N_K:
        pass
    elif len(key_generating_key) == 4 * AES_256_N_K:
        pass
    else:
        raise ValueError("KGK is neither 16 or 32 bytes long", len(key_generating_key))
    if len(number_used_once) != 12:
        raise ValueError("Number used once is not 96 bits", len(number_used_once * 8))

    _little_endian_uint32_0 = 0x00.to_bytes(
        4,
        byteorder="little",
        signed=False,
    )
    _little_endian_uint32_1 = 0x01.to_bytes(
        4,
        byteorder="little",
        signed=False,
    )

    _p_0 = _little_endian_uint32_0 + number_used_once
    (_mak_1st_block,) = aes_func(k=key_generating_key, p=_p_0)
    _p_1 = _little_endian_uint32_1 + number_used_once
    (_mak_2nd_block,) = aes_func(k=key_generating_key, p=_p_1)

    _message_authentication_key = _mak_1st_block[:8] + _mak_2nd_block[:8]

    _little_endian_uint32_2 = 0x02.to_bytes(
        4,
        byteorder="little",
        signed=False,
    )
    _little_endian_uint32_3 = 0x03.to_bytes(
        4,
        byteorder="little",
        signed=False,
    )

    _p_2 = _little_endian_uint32_2 + number_used_once
    (_mek_1st_block,) = aes_func(k=key_generating_key, p=_p_2)
    _p_3 = _little_endian_uint32_3 + number_used_once
    (_mek_2nd_block,) = aes_func(k=key_generating_key, p=_p_3)

    _message_encryption_key = _mek_1st_block[:8] + _mek_2nd_block[:8]

    _debug_values = {
        "mak_1st_block": _mak_1st_block,
        "mak_2nd_block": _mak_2nd_block,
        "mek_1st_block": _mek_1st_block,
        "mek_2nd_block": _mek_2nd_block,
    }

    if len(key_generating_key) == 32:

        _little_endian_uint32_4 = 0x04.to_bytes(
            4,
            byteorder="little",
            signed=False,
        )
        _little_endian_uint32_5 = 0x05.to_bytes(
            4,
            byteorder="little",
            signed=False,
        )

        _p_4 = _little_endian_uint32_4 + number_used_once
        (_mek_3rd_block,) = aes_func(k=key_generating_key, p=_p_4)
        _p_5 = _little_endian_uint32_5 + number_used_once
        (_mek_4th_block,) = aes_func(k=key_generating_key, p=_p_5)

        _message_encryption_key += _mek_3rd_block[:8] + _mek_4th_block[0:8]

        _debug_values = {
            "mek_3rd_block": _mek_3rd_block,
            "mek_4th_block": _mek_4th_block,
        }

    _return = (
        _message_authentication_key,
        _message_encryption_key,
        _debug_values,
    )

    return _return


def right_pad_to_multiple_of_16_bytes(
    to_pad: bytes,
) -> bytes:
    """Plain text padding function.
    RFC 8452 section 4.
    """
    _padded = copy.deepcopy(to_pad)
    while len(_padded) % 16 != 0:
        _padded += b"\x00"

    return _padded


def aes_gcm_siv_encrypt(
    key_generating_key: bytes,
    number_used_once: bytes,
    plaintext: bytes,
    additional_data: bytes,
    aes_ecb_function,
    aes_ctr_function,
) -> tuple[bytes, dict]:
    """GCM SIV encrypt function.
    RFC 8452 section 4.
    """
    if len(plaintext) > AEAD_AES_128_GCM_SIV_P_MAX:
        raise ValueError("Plaintext is too long.", AEAD_AES_128_GCM_SIV_P_MAX, len(plaintext))
    if len(additional_data) > AEAD_AES_128_GCM_SIV_A_MAX:
        raise ValueError("Additional data is too long.", AEAD_AES_128_GCM_SIV_A_MAX, len(additional_data))

    if aes_ecb_function == aes128_ecb_encrypt:
        if aes_ctr_function != aes128_ctr_encrypt:
            raise ValueError("Incorrect AES-CTR functions.", aes_ctr_function)
    elif aes_ecb_function == aes256_ecb_encrypt:
        if aes_ctr_function != aes256_ctr_encrypt:
            raise ValueError("Incorrect AES-CTR functions.", aes_ctr_function)
    else:
        raise ValueError("Incorrect AES-ECB function.", aes_ecb_function)

    (
        _mak,
        _mek,
        _derive_keys_debug_values,
    ) = derive_keys(
        key_generating_key,
        number_used_once,
        aes_ecb_function,
    )

    _little_endian_uint64_ad_len_bits = (len(additional_data) * 8).to_bytes(
        8,
        byteorder="little",
        signed=False,
    )
    _little_endian_uint64_pt_len_bits = (len(plaintext) * 8).to_bytes(
        8,
        byteorder="little",
        signed=False,
    )
    _length_block = _little_endian_uint64_ad_len_bits + _little_endian_uint64_pt_len_bits

    _padded_pt = right_pad_to_multiple_of_16_bytes(plaintext)
    _padded_ad = right_pad_to_multiple_of_16_bytes(additional_data)

    (
        _s_s,
        _polyval_debug_values,
    ) = polyval(
        _mak,
        _padded_ad + _padded_pt + _length_block,
    )
    for _i in range(0, AEAD_AES_128_GCM_SIV_N_MIN):
        _s_s[_i] ^= number_used_once[_i]
    _s_s[15] &= 0x7F
    (_tag,) = aes_ecb_function(
        _mek,
        _s_s,
    )

    _counter_block = bytearray(_tag)
    _counter_block[15] |= 0x80

    (
        _ciphertext,
        _aes_ctr_debug_values,
    ) = aes_ctr_function(
        k=_mek,
        m_len_bytes=4,
        m_be_bool=False,
        t_1=_counter_block,
        p_len_bits=len(plaintext) * 8,
        p=plaintext,
    )

    _debug = {
        "tag": _tag,
        "mak": _mak,
        "mek": _mek,
        "derive_keys_debug_values": _derive_keys_debug_values,
        "polyval_debug_values": _polyval_debug_values,
        "aes_ctr_debug_values": _aes_ctr_debug_values,
    }

    _return = (
        _ciphertext,
        _debug,
    )

    return _return


def aes_gcm_siv_decrypt(
    key_generating_key: bytes,
    number_used_once: bytes,
    ciphertext: bytes,
    additional_data: bytes,
    aes_ecb_function,
    aes_ctr_function,
) -> tuple[bytes, dict]:
    """GCM SIV decrypt function.
    RFC 8452 section 5.
    """
    if len(ciphertext) > AEAD_AES_128_GCM_SIV_P_MAX:
        raise ValueError("Plaintext is too long.", AEAD_AES_128_GCM_SIV_P_MAX, len(ciphertext))
    if len(additional_data) > AEAD_AES_128_GCM_SIV_A_MAX:
        raise ValueError("Additional data is too long.", AEAD_AES_128_GCM_SIV_A_MAX, len(additional_data))

    if aes_ecb_function == aes128_ecb_encrypt:
        if aes_ctr_function != aes128_ctr_decrypt:
            raise ValueError("Incorrect AES-CTR functions.", aes_ctr_function)
    elif aes_ecb_function == aes256_ecb_encrypt:
        if aes_ctr_function != aes256_ctr_decrypt:
            raise ValueError("Incorrect AES-CTR functions.", aes_ctr_function)
    else:
        raise ValueError("Incorrect AES-ECB function.", aes_ecb_function)

    (
        _mak,
        _mek,
        _derive_keys_debug_values,
    ) = derive_keys(
        key_generating_key,
        number_used_once,
        aes_ecb_function,
    )

    _tag = ciphertext[-16:]

    _counter_block = bytearray(_tag)
    _counter_block[15] |= 0x80

    (
        _plaintext,
        _aes_ctr_debug_values,
    ) = aes_ctr_function(
        k=_mek,
        m_len_bytes=4,
        m_be_bool=False,
        t_1=_counter_block,
        c_len_bits=len(ciphertext) * 8 - 16,
        c=ciphertext[:-16],
    )

    _little_endian_uint64_ad_len_bits = (len(additional_data) * 8).to_bytes(
        8,
        byteorder="little",
        signed=False,
    )
    _little_endian_uint64_pt_len_bits = (len(_plaintext) * 8).to_bytes(
        8,
        byteorder="little",
        signed=False,
    )
    _length_block = _little_endian_uint64_ad_len_bits + _little_endian_uint64_pt_len_bits

    _padded_pt = right_pad_to_multiple_of_16_bytes(_plaintext)
    _padded_ad = right_pad_to_multiple_of_16_bytes(additional_data)

    (
        _s_s,
        _polyval_debug_values,
    ) = polyval(
        _mak,
        _padded_ad + _padded_pt + _length_block,
    )
    for _i in range(0, AEAD_AES_128_GCM_SIV_N_MIN):
        _s_s[_i] ^= number_used_once[_i]
    _s_s[15] &= 0x7F
    (_expected_tag,) = aes_ecb_function(
        _mek,
        _s_s,
    )

    _xor_sum = 0
    for i, _ in enumerate(_expected_tag):
        _xor_sum |= _expected_tag[i] ^ _tag[i]

    if _xor_sum != 0:
        raise ValueError("Tag is not a match, failed authenticated decryption.", _tag.hex(), _expected_tag.hex())

    _debug = {
        "mak": _mak,
        "mek": _mek,
        "derive_keys_debug_values": _derive_keys_debug_values,
        "polyval_debug_values": _polyval_debug_values,
        "aes_ctr_debug_values": _aes_ctr_debug_values,
    }

    _return = (
        _plaintext,
        _debug,
    )

    return _return

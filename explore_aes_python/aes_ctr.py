"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

Ref: https://csrc.nist.gov/pubs/sp/800/38/a/final
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_128_N_B,
    AES_128_N_K,
    AES_128_N_R,
    AES_192_N_B,
    AES_192_N_K,
    AES_192_N_R,
    AES_256_N_B,
    AES_256_N_K,
    AES_256_N_R,
    AES_128_RCON_BYTES,
    AES_192_RCON_BYTES,
    AES_256_RCON_BYTES,
)
from explore_aes_python.aes_functions import (
    cipher_bytes,
)
from explore_aes_python.aes_key_expansion import key_expansion_bytes


def aes_ctr(
    k: bytes,
    m_len_bytes: int,
    m_be_bool,
    t_1: bytes,
    message_len_bits: int,
    message: bytearray,
    n_k: int,
    n_r: int,
    n_b: int = 4,
) -> tuple[bytes, dict]:
    """Counter mode cipher (decrypt or encrypt).
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    RFC 8452, April 2019, section 4.
    Note that GCM SIV defines the counterblock with a 32 bit counter, but as a
    little endian value in the left most bytes of the counter block.
    The parameter m_be if True defines a big endian counter in the right-most bytes.
    The parameter m_be if False defines a little endian counter in the left-most bytes.
    """
    # Algorithm parameters must be consistent.
    # Selected Nk defines the appropriate round constants.
    if n_k == AES_128_N_K:
        if len(k) != 4 * AES_128_N_K:
            raise ValueError("Incorrect key length", len(k), n_k)
        if n_r != AES_128_N_R:
            raise ValueError("Incorrect Nr", n_r)
        if n_b != AES_128_N_B:
            raise ValueError("Incorrect Nb", n_b)
        _rcon = AES_128_RCON_BYTES
    elif n_k == AES_192_N_K:
        if len(k) != 4 * AES_192_N_K:
            raise ValueError("Incorrect key length", len(k), n_k)
        if n_r != AES_192_N_R:
            raise ValueError("Incorrect Nr", n_r)
        if n_b != AES_192_N_B:
            raise ValueError("Incorrect Nb", n_b)
        _rcon = AES_192_RCON_BYTES
    elif n_k == AES_256_N_K:
        if len(k) != 4 * AES_256_N_K:
            raise ValueError("Incorrect key length", len(k), n_k)
        if n_r != AES_256_N_R:
            raise ValueError("Incorrect Nr", n_r)
        if n_b != AES_256_N_B:
            raise ValueError("Incorrect Nb", n_b)
        _rcon = AES_256_RCON_BYTES
    else:
        raise ValueError("Cipher incorrect Nk value", n_k)

    # Initial counter block T1 must match selected block length Nb.
    # Counter bit width m must be 0 < m < b where b is block length in bits.
    # Message must be maximum 2**m blocks in length.
    if len(t_1) != (4 * n_b):
        raise ValueError("Incorrect initial_ctr_block length", len(t_1))
    if (m_len_bytes < 1) or (m_len_bytes > len(t_1) * 8):
        raise ValueError("Incorrect ctr_len_bits values", m_len_bytes, len(t_1) * 8)
    if len(message) > (4 * n_b) * (2**m_len_bytes):
        raise ValueError("Message too long", len(message), (4 * n_b), (2**m_len_bytes))

    _message_dash = bytearray()
    _message_debug_results = {
        "initial_ctr_block": t_1,
        "ctr_blocks": [],
        "cipher_stream_blocks": [],
        "message_blocks": [],
        "message_dash_blocks": [],
    }

    # Extract the t_1 counter block counter bytes.
    # These may then be used to increment the counter and construct each subsequent t_n block.
    if m_be_bool:
        _m_bytes = t_1[-m_len_bytes:]
        _m_int = int.from_bytes(
            _m_bytes,
            byteorder="big",
            signed=False,
        )
    else:
        _m_bytes = t_1[:m_len_bytes]
        _m_int = int.from_bytes(
            _m_bytes,
            byteorder="little",
            signed=False,
        )

    # Initialise the conter block and round keys variables.
    (
        _w,
        _,
    ) = key_expansion_bytes(
        k,
        _rcon,
        n_k,
        n_r,
        n_b,
    )

    # Process each full block in the messsage.
    _message_len_blocks = message_len_bits // (8 * 4 * n_b)
    _block = 0
    for _block in range(0, _message_len_blocks):
        # Make the counter block into a bytes like object.
        # Cipher the counter block to create a cipher stream block.
        if m_be_bool:
            _m_bytes = _m_int.to_bytes(
                m_len_bytes,
                byteorder="big",
                signed=False,
            )
            _t_bytes = t_1[:-m_len_bytes] + _m_bytes
        else:
            _m_bytes = _m_int.to_bytes(
                m_len_bytes,
                byteorder="little",
                signed=False,
            )
            _t_bytes = _m_bytes + t_1[m_len_bytes:]

        (_cipher_stream_block, _) = cipher_bytes(
            _t_bytes,
            _w,
            n_k,
            n_r,
            n_b,
        )

        # Encrypt the message block with the cipher stream block.
        _message_block = message[_block * 4 * n_b : (_block + 1) * 4 * n_b]
        _message_dash_block = bytearray(_m ^ _k for _m, _k in zip(_message_block, _cipher_stream_block))

        _message_dash += _message_dash_block

        # Increment the counter ready for the next block or last block/few bits.
        _m_int += 1

        # Stash the per block interim results for debug and test purposes.
        _message_debug_results["ctr_blocks"].append(_t_bytes)
        _message_debug_results["cipher_stream_blocks"].append(_cipher_stream_block)
        _message_debug_results["message_blocks"].append(_message_block)
        _message_debug_results["message_dash_blocks"].append(_message_dash_block)

    # Process the final block if the message is not 'mod (block length bits) = 0' long.
    _message_last_bits = message_len_bits % (8 * 4 * n_b)
    if _message_last_bits != 0:
        # Again make the counter block into a bytes like object.
        # Again cipher the counter block to create a cipher stream block.
        _block = _message_len_blocks
        if m_be_bool:
            _m_bytes = _m_int.to_bytes(
                m_len_bytes,
                byteorder="big",
                signed=False,
            )
            _t_bytes = t_1[:-m_len_bytes] + _m_bytes
        else:
            _m_bytes = _m_int.to_bytes(
                m_len_bytes,
                byteorder="little",
                signed=False,
            )
            _t_bytes = _m_bytes + t_1[m_len_bytes:]

        (
            _cipher_stream_block,
            _,
        ) = cipher_bytes(
            _t_bytes,
            _w,
            n_k,
            n_r,
            n_b,
        )

        # Again encrypt the message block with the cipher stream block.
        _message_block = message[(_block) * 4 * n_b : (_block + 1) * 4 * n_b]
        _message_dash_block = bytearray(_m ^ _k for _m, _k in zip(_message_block, _cipher_stream_block))

        # Zero the trailing bits that are not part of the message.
        _message_len_mask = 2 ** (8 * 4 * n_b) - 1
        _message_len_mask ^= 2 ** ((8 * 4 * n_b) - _message_last_bits) - 1
        _message_len_mask = _message_len_mask.to_bytes(
            4 * n_b,
            byteorder="big",
            signed=False,
        )
        _message_dash_last_block = bytearray(_m & _mask for _m, _mask in zip(_message_dash_block, _message_len_mask))
        _message_dash += _message_dash_last_block[0 : (_message_last_bits + 7) // 8]

        # Again stash the per block interim results for debug and test purposes.
        _message_debug_results["ctr_blocks"].append(_t_bytes)
        _message_debug_results["cipher_stream_blocks"].append(_cipher_stream_block)
        _message_debug_results["message_blocks"].append(_message_block)
        _message_debug_results["message_dash_blocks"].append(_message_dash_block)

    _return = (
        _message_dash,
        _message_debug_results,
    )

    return _return


def aes128_ctr_encrypt(
    k: bytes,
    m_len_bytes: int,
    m_be_bool: bool,
    t_1: bytes,
    p_len_bits: int,
    p: bytearray,
) -> tuple[bytes, dict]:
    """CTR mode encryption, using AES128 block cipher.
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    """
    (
        _c,
        _debug_values,
    ) = aes_ctr(
        k,
        m_len_bytes,
        m_be_bool,
        t_1,
        p_len_bits,
        p,
        n_k=AES_128_N_K,
        n_r=AES_128_N_R,
        n_b=AES_128_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes128_ctr_decrypt(
    k: bytes,
    m_len_bytes: int,
    m_be_bool: bool,
    t_1: bytes,
    c_len_bits: int,
    c: bytearray,
) -> tuple[bytes, dict]:
    """CTR mode decryption, using AES128 block cipher.
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    """
    (
        _c,
        _debug_values,
    ) = aes_ctr(
        k,
        m_len_bytes,
        m_be_bool,
        t_1,
        c_len_bits,
        c,
        n_k=AES_128_N_K,
        n_r=AES_128_N_R,
        n_b=AES_128_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes192_ctr_encrypt(
    k: bytes,
    m_len_bytes: int,
    m_be_bool: bool,
    t_1: bytes,
    p_len_bits: int,
    p: bytearray,
) -> tuple[bytes, dict]:
    """CTR mode encryption, using AES192 block cipher.
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    """
    (
        _c,
        _debug_values,
    ) = aes_ctr(
        k,
        m_len_bytes,
        m_be_bool,
        t_1,
        p_len_bits,
        p,
        n_k=AES_192_N_K,
        n_r=AES_192_N_R,
        n_b=AES_192_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes192_ctr_decrypt(
    k: bytes,
    m_len_bytes: int,
    m_be_bool: bool,
    t_1: bytes,
    c_len_bits: int,
    c: bytearray,
) -> tuple[bytes, dict]:
    """CTR mode decryption, using AES192 block cipher.
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    """
    (
        _p,
        _debug_values,
    ) = aes_ctr(
        k,
        m_len_bytes,
        m_be_bool,
        t_1,
        c_len_bits,
        c,
        n_k=AES_192_N_K,
        n_r=AES_192_N_R,
        n_b=AES_192_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes256_ctr_encrypt(
    k: bytes,
    m_len_bytes: int,
    m_be_bool: bool,
    t_1: bytes,
    p_len_bits: int,
    p: bytearray,
) -> tuple[bytes, dict]:
    """CTR mode encryption, using AES256 block cipher.
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    """
    (
        _p,
        _debug_values,
    ) = aes_ctr(
        k,
        m_len_bytes,
        m_be_bool,
        t_1,
        p_len_bits,
        p,
        n_k=AES_256_N_K,
        n_r=AES_256_N_R,
        n_b=AES_256_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes256_ctr_decrypt(
    k: bytes,
    m_len_bytes: int,
    m_be_bool: bool,
    t_1: bytes,
    c_len_bits: int,
    c: bytearray,
) -> tuple[bytes, dict]:
    """CTR mode decryption, using AES256 block cipher.
    NIST SP 800-38A, December 2001, section 6.5; The Counter Mode.
    NIST SP 800-38A, December 2001, Appendix B; Generation of counter blocks.
    Note that the second recomended method is assumed here,
      a 'number used once' occupies b-m upper bits of the initial counter block,
      and an m bit counter occupying the lower m bits is incremented for each
      block of the message.
    """
    (
        _p,
        _debug_values,
    ) = aes_ctr(
        k,
        m_len_bytes,
        m_be_bool,
        t_1,
        c_len_bits,
        c,
        n_k=AES_256_N_K,
        n_r=AES_256_N_R,
        n_b=AES_256_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return

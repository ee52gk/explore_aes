"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

Ref: https://csrc.nist.gov/pubs/sp/800/38/a/final
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_128_RCON_BYTES,
    AES_192_RCON_BYTES,
    AES_256_RCON_BYTES,
    AES_128_N_K,
    AES_192_N_K,
    AES_256_N_K,
    AES_128_N_B,
    AES_192_N_B,
    AES_256_N_B,
    AES_128_N_R,
    AES_192_N_R,
    AES_256_N_R,
)
from explore_aes_python.aes_key_expansion import (
    key_expansion_bytes,
)
from explore_aes_python.aes_functions import (
    cipher_bytes,
)


def aes_ofb_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
    r_con: bytes,
    n_k: int,
    n_r: int,
    n_b: int = 4,
) -> tuple[bytes, dict]:
    """Wrapper function providing a parameterised AES-OFB mode encrypt."""
    if len(k) % (4 * n_k) != 0:
        raise ValueError("Key length is not 4 * 8 * n_k bits", n_k, len(k) * 8)
    if len(iv) != (4 * n_b):
        raise ValueError("OFB IV is not a block length", len(iv), iv.hex())
    if len(p) % (4 * n_b) != 0:
        raise ValueError("Plain text length is not a block length multiple", n_b, len(p))

    (
        _w,
        _,
    ) = key_expansion_bytes(
        key=k,
        r_con=r_con,
        n_k=n_k,
        n_r=n_r,
        n_b=n_b,
    )

    _o_jm1 = iv
    _c = bytearray()
    _debug_values = {"_o_j": []}
    for _j in range(0, len(p), 4 * n_b):
        (
            _o_j,
            _,
        ) = cipher_bytes(
            block=_o_jm1,
            w=_w,
            n_k=n_k,
            n_r=n_r,
            n_b=n_b,
        )
        _debug_values["_o_j"].append(_o_j)
        _c_j = bytes(x ^ y for x, y in zip(_o_j, p[_j : _j + 4 * n_b]))
        _c += _c_j
        _o_jm1 = _o_j

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes_ofb_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
    r_con: bytes,
    n_k: int,
    n_r: int,
    n_b: int = 4,
) -> tuple[bytes, dict]:
    """Wrapper function providing a parameterised AES-OFC mode decrypt."""
    if len(k) % (4 * n_k) != 0:
        raise ValueError("Key length is not 4 * 8 * n_k bits", n_k, len(k) * 8)
    if len(iv) != (4 * n_b):
        raise ValueError("OFB IV is not a block length", len(iv), iv.hex())
    if len(c) % (4 * n_b) != 0:
        raise ValueError("Cipher text length is not a block length multiple", n_b, len(c))

    (
        _w,
        _,
    ) = key_expansion_bytes(
        key=k,
        r_con=r_con,
        n_k=n_k,
        n_r=n_r,
        n_b=n_b,
    )

    _o_jm1 = iv
    _p = bytearray()
    _debug_values = {"_o_j": []}
    for _j in range(0, len(c), 4 * n_b):
        (
            _o_j,
            _,
        ) = cipher_bytes(
            block=_o_jm1,
            w=_w,
            n_k=n_k,
            n_r=n_r,
            n_b=n_b,
        )
        _debug_values["_o_j"].append(_o_j)

        _p_j = bytes(x ^ y for x, y in zip(_o_j, c[_j : _j + 4 * n_b]))
        _p += _p_j
        _o_jm1 = _o_j

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes128_ofb_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES128-OFB mode encrypt."""
    (
        _c,
        _debug_values,
    ) = aes_ofb_encrypt(
        k=k,
        iv=iv,
        p=p,
        r_con=AES_128_RCON_BYTES,
        n_k=AES_128_N_K,
        n_r=AES_128_N_R,
        n_b=AES_128_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes128_ofb_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES128-OFB mode decrypt."""
    (
        _p,
        _debug_values,
    ) = aes_ofb_decrypt(
        k=k,
        iv=iv,
        c=c,
        r_con=AES_128_RCON_BYTES,
        n_k=AES_128_N_K,
        n_r=AES_128_N_R,
        n_b=AES_128_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes192_ofb_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES192-OFB mode encrypt."""
    (
        _c,
        _debug_values,
    ) = aes_ofb_encrypt(
        k=k,
        iv=iv,
        p=p,
        r_con=AES_192_RCON_BYTES,
        n_k=AES_192_N_K,
        n_r=AES_192_N_R,
        n_b=AES_192_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes192_ofb_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES192-OFB mode decrypt."""
    (
        _p,
        _debug_values,
    ) = aes_ofb_decrypt(
        k=k,
        iv=iv,
        c=c,
        r_con=AES_192_RCON_BYTES,
        n_k=AES_192_N_K,
        n_r=AES_192_N_R,
        n_b=AES_192_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes256_ofb_encrypt(
    k: bytes,
    iv: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES256-OFB mode encrypt."""
    (
        _c,
        _debug_values,
    ) = aes_ofb_encrypt(
        k=k,
        iv=iv,
        p=p,
        r_con=AES_256_RCON_BYTES,
        n_k=AES_256_N_K,
        n_r=AES_256_N_R,
        n_b=AES_256_N_B,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes256_ofb_decrypt(
    k: bytes,
    iv: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Wrapper function providing a AES256-OFB mode decrypt."""
    (
        _p,
        _debug_values,
    ) = aes_ofb_decrypt(
        k=k,
        iv=iv,
        c=c,
        r_con=AES_256_RCON_BYTES,
        n_k=AES_256_N_K,
        n_r=AES_256_N_R,
        n_b=AES_256_N_B,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return

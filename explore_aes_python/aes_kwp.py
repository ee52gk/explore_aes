"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2024

Ref: https://csrc.nist.gov/pubs/sp/800/38/f/final
Ref: https://datatracker.ietf.org/doc/html/rfc5649
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_ecb import (
    aes128_ecb_decrypt,
    aes128_ecb_encrypt,
    aes192_ecb_decrypt,
    aes192_ecb_encrypt,
    aes256_ecb_decrypt,
    aes256_ecb_encrypt,
)
from explore_aes_python.aes_kw import (
    nist_800_38f_aes_w,
    nist_800_38f_aes_w_inv,
)


BLOCK_LEN_BYTES = 16
SEMIBLOCK_LEN_BYTES = BLOCK_LEN_BYTES // 2


def aes_kwp_ae(
    k: bytes,
    aes_ecb_encrypt,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_encrypt (_type_): The AES-ECB block cipher function to use.
        p (bytes): The plaintext.

    Raises:
        ValueError: p is an invalid length; not an integer multiple of a semiblock length.
        ValueError: p is not at least 3 semiblocks in length.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    _p_len_bytes = len(p)
    _p_len_bits = _p_len_bytes * 8

    _icv2_hex = "A65959A6"
    _icv2_bytes = bytes.fromhex(_icv2_hex)
    _pad_len = 8 * ((_p_len_bits + 63) // 64) - (_p_len_bits // 8)
    _pad = b"\x00" * _pad_len
    _p_len_bytes = ((_p_len_bits + 7) // 8).to_bytes(
        4,
        byteorder="big",
        signed=False,
    )
    _s = _icv2_bytes + _p_len_bytes + p + _pad

    _c = None
    _debug_values = None
    if _p_len_bits <= 64:
        (_c,) = aes_ecb_encrypt(
            k,
            _s,
        )
        _debug_values = {}
    elif _p_len_bits > 64:
        (
            _c,
            _debug_values,
        ) = nist_800_38f_aes_w(
            k,
            aes_ecb_encrypt,
            _s,
        )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes_kwp_ad(
    k: bytes,
    aes_ecb_decrypt,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        aes_ecb_encrypt (_type_): The AES-ECB block cipher function to use.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    _c_len_bytes = len(c)
    _c_len_bits = len(c) * 8
    if _c_len_bytes % SEMIBLOCK_LEN_BYTES != 0:
        raise ValueError(
            "C is not an integer multiple of the semiblock length.", _c_len_bytes, _c_len_bytes % SEMIBLOCK_LEN_BYTES
        )
    if _c_len_bytes // SEMIBLOCK_LEN_BYTES < 2:
        raise ValueError(
            "C is not at least 2 semiblocks in length.", _c_len_bytes, _c_len_bytes // SEMIBLOCK_LEN_BYTES
        )

    _n = _c_len_bytes // SEMIBLOCK_LEN_BYTES

    _icv2_hex = "A65959A6"
    _icv2_bytes = bytes.fromhex(_icv2_hex)

    _s = None
    _debug_values = None
    if _n == 2:
        (_s,) = aes_ecb_decrypt(
            k,
            c,
        )
        _debug_values = {}
    elif _n > 2:
        (
            _s,
            _debug_values,
        ) = nist_800_38f_aes_w_inv(
            k,
            aes_ecb_decrypt,
            c,
        )

    _icv_check = _s[: SEMIBLOCK_LEN_BYTES // 2]
    if _icv_check == _icv2_bytes:
        _p_len_int = int.from_bytes(_s[:SEMIBLOCK_LEN_BYTES][SEMIBLOCK_LEN_BYTES // 2 :])
        _pad_len = 8 * (_n - 1) - _p_len_int
        if _pad_len < 0 or _pad_len > 7:
            _p = None
        elif _s[len(_s) - _pad_len :] != b"\x00" * _pad_len:
            _p = None
        else:
            _p = _s[-8 * (_n - 1) :][:_p_len_int]
    else:
        _p = None

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes128_kwp_ae(
    k: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        p (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    (
        _c,
        _debug_values,
    ) = aes_kwp_ae(
        k,
        aes128_ecb_encrypt,
        p,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes192_kwp_ae(
    k: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        p (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    (
        _c,
        _debug_values,
    ) = aes_kwp_ae(
        k,
        aes192_ecb_encrypt,
        p,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes256_kwp_ae(
    k: bytes,
    p: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Encryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        p (bytes): The plaintext.

    Returns:
        tuple[bytes, dict]: The ciphertext, c, and a dictionary incl. named debug values & arrays.
    """
    (
        _c,
        _debug_values,
    ) = aes_kwp_ae(
        k,
        aes256_ecb_encrypt,
        p,
    )

    _return = (
        _c,
        _debug_values,
    )

    return _return


def aes128_kwp_ad(
    k: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    (
        _p,
        _debug_values,
    ) = aes_kwp_ad(
        k,
        aes128_ecb_decrypt,
        c,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes192_kwp_ad(
    k: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    (
        _p,
        _debug_values,
    ) = aes_kwp_ad(
        k,
        aes192_ecb_decrypt,
        c,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return


def aes256_kwp_ad(
    k: bytes,
    c: bytes,
) -> tuple[bytes, dict]:
    """Key Wrap with Padding Authenticated Decryption function.

    Args:
        k (bytes): The cipher key, must match aes_ecb_encrypt, e.g. 128-bits for AES128.
        c (bytes): The ciphertext.

    Returns:
        tuple[bytes, dict]: The plaintext, p, and a dictionary incl. named debug values & arrays.
                            p is empty if the decryption fails authentication.
    """
    (
        _p,
        _debug_values,
    ) = aes_kwp_ad(
        k,
        aes256_ecb_decrypt,
        c,
    )

    _return = (
        _p,
        _debug_values,
    )

    return _return

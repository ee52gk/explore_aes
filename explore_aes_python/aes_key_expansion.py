"""
 ▄▄▄▄▄▄▄▄  ▄      ▄  ▄▄▄▄▄▄▄▄            ▄▄▄▄▄▄▄▄  ▄      ▄
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌▌   ▐░▌▐░█▀▀▀▀▀▀            ▀▀▀██▀▀▀ ▐░▌▌   ▐░▌
▐░▌       ▐░▌▐   ▐░▌▐░▌                     ▐▌    ▐░▌▐   ▐░▌
▐░█▄▄▄▄▄▄ ▐░▌ ▌  ▐░▌▐░▌ ▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄   ▐▌    ▐░▌ ▌  ▐░▌
▐░░░░░░░░▌▐░▌ ██ ▐░▌▐░▌▐░░░░░▌▐░░░░░░░░░░▌  ▐▌    ▐░▌ ██ ▐░▌
▐░█▀▀▀▀▀▀ ▐░▌  ▐ ▐░▌▐░▌ ▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀▀   ▐▌    ▐░▌  ▐ ▐░▌
▐░▌       ▐░▌   ▌▐░▌▐░▌    ▐░▌              ▐▌    ▐░▌   ▌▐░▌
▐░█▄▄▄▄▄▄ ▐░▌   ▐▐░▌▐░█▄▄▄▄█░▌           ▄▄▄██▄▄▄ ▐░▌   ▐▐░▌
▐░░░░░░░░▌▐░▌    ▐░▌▐░░░░░░░░▌          ▐░░░░░░░░▌▐░▌    ▐░▌
 ▀▀▀▀▀▀▀▀  ▀      ▀  ▀▀▀▀▀▀▀▀            ▀▀▀▀▀▀▀▀  ▀      ▀
     45       6E        67        2D        49        6E
 01000101  01101110  01100111  00101101  01001001  01101110
           ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄  ▄      ▄
          ▐░░░░░░░░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
          ▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░█▀▀▀▀█░▌▐░▌▌  ▐▐░▌
          ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌▐░░▌ ░▌
          ▐░█▄▄▄▄█░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░░░░░░░░▌▐░▌    ▐░▌▐░▌    ▐░▌▐░▌ ▐▌ ▐░▌
          ▐░█▀█░█▀▀ ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌  ▐░▌  ▐░▌    ▐░▌▐░▌    ▐░▌▐░▌    ▐░▌
          ▐░▌   ▐░▌ ▐░█▄▄▄▄█░▌▐░█▄▄▄▄█░▌▐░▌    ▐░▌
          ▐░▌    ▐░▌▐░░░░░░░░▌▐░░░░░░░░▌▐░▌    ▐░▌
           ▀      ▀  ▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀  ▀      ▀
              52        6F        6F        6D
           01010010  01101111  01101111  01101101

Logo edited from generated source:
    https://patorjk.com/software/taag/#p=display&f=Electronic&t=EngIn%20Room

(C) Geoff Krechting 2021

Ref: https://csrc.nist.gov/pubs/fips/197/final
"""

# Standard imports.


# 3rd party imports.


# Submodule imports.


# Local imports.
from explore_aes_python.aes_constants import (
    AES_256_N_K,
    AES_192_N_K,
    AES_128_N_K,
    AES_256_N_B,
    AES_192_N_B,
    AES_128_N_B,
    AES_256_N_R,
    AES_192_N_R,
    AES_128_N_R,
    sbox_int,
)
from explore_aes_python.aes_functions import (
    inv_mix_columns_bytes,
)


def rotword_bytes(
    word: bytes,
) -> list:
    """Byte wise cycling transform.
    FIPS 197 section 5.2
    [a0,a1,a2,a3] => [a1,a2,a3,a0]
    Where a0, a1, s2, a4 are 8-bit values.
    """
    if len(word) != 4:
        raise ValueError("Rotword bytes not len 4", len(word))
    for i in range(1, 4):
        if (word[i] < 0) and (word[i] > 2**8):
            raise ValueError("Rotword byte out of range", word[i])

    _rotword = word[1:4] + word[0:1]

    return _rotword


def subword_bytes(
    word: bytes,
) -> bytearray:
    """Byte wise sbox substitution transform.
    FIPS 197 section 5.2
    """
    _subword = bytearray.fromhex("00" * len(word))
    for i, _ in enumerate(word):
        _subword[i] = sbox_int(word[i])

    return _subword


def key_expansion_bytes(
    key: bytes,
    r_con: bytes,
    n_k: int,
    n_r: int,
    n_b: int = 4,
) -> tuple[bytes, dict]:
    """Function to expand AES key for use in the Cipher and Inverse Cipher."""
    if (n_k != AES_256_N_K) and (n_k != AES_192_N_K) and (n_k != AES_128_N_K):
        raise ValueError("incorrect Nk:", n_k)
    if (n_b != AES_256_N_B) and (n_b != AES_192_N_B) and (n_b != AES_128_N_B):
        raise ValueError("incorrect Nb:", n_b)
    if (n_r != AES_256_N_R) and (n_r != AES_192_N_R) and (n_r != AES_128_N_R):
        raise ValueError("incorrect Nr:", n_r)
    if len(key) != 4 * n_k:
        raise ValueError("incorrect key length, Nk:", len(key), n_k)

    _w = bytearray()
    _temp1 = bytearray()
    _rotword = bytearray()
    _subword = bytearray()
    _temp2 = bytearray()

    for _ in range(0, n_b * (n_r + 1)):
        _w += b"\x00\x00\x00\x00"
        _temp1 += b"\x00\x00\x00\x00"
        _rotword += b"\x00\x00\x00\x00"
        _subword += b"\x00\x00\x00\x00"
        _temp2 += b"\x00\x00\x00\x00"

    i = 0
    while i < n_k:
        _w[i * 4 + 0] = key[i * 4 + 0]
        _w[i * 4 + 1] = key[i * 4 + 1]
        _w[i * 4 + 2] = key[i * 4 + 2]
        _w[i * 4 + 3] = key[i * 4 + 3]
        i = i + 1

    i = n_k
    while i < (n_b * (n_r + 1)):
        _temp1[i * 4 + 0] = _w[(i - 1) * 4 + 0]
        _temp1[i * 4 + 1] = _w[(i - 1) * 4 + 1]
        _temp1[i * 4 + 2] = _w[(i - 1) * 4 + 2]
        _temp1[i * 4 + 3] = _w[(i - 1) * 4 + 3]

        if (i % n_k) == 0:
            temp_rotword = rotword_bytes(_temp1[i * 4 + 0 : i * 4 + 4])
            _rotword[i * 4 + 0] = temp_rotword[0]
            _rotword[i * 4 + 1] = temp_rotword[1]
            _rotword[i * 4 + 2] = temp_rotword[2]
            _rotword[i * 4 + 3] = temp_rotword[3]

            temp_subword = subword_bytes(_rotword[i * 4 + 0 : i * 4 + 4])
            _subword[i * 4 + 0] = temp_subword[0]
            _subword[i * 4 + 1] = temp_subword[1]
            _subword[i * 4 + 2] = temp_subword[2]
            _subword[i * 4 + 3] = temp_subword[3]

            _temp2[i * 4 + 0] = _subword[i * 4 + 0] ^ r_con[(i // n_k) * 4 + 0]
            _temp2[i * 4 + 1] = _subword[i * 4 + 1] ^ r_con[(i // n_k) * 4 + 1]
            _temp2[i * 4 + 2] = _subword[i * 4 + 2] ^ r_con[(i // n_k) * 4 + 2]
            _temp2[i * 4 + 3] = _subword[i * 4 + 3] ^ r_con[(i // n_k) * 4 + 3]
        elif (n_k > 6) and ((i % n_k) == 4):
            temp_subword = subword_bytes(_temp1[i * 4 + 0 : i * 4 + 4])
            _temp2[i * 4 + 0] = temp_subword[0]
            _temp2[i * 4 + 1] = temp_subword[1]
            _temp2[i * 4 + 2] = temp_subword[2]
            _temp2[i * 4 + 3] = temp_subword[3]
        else:
            _temp2[i * 4 + 0] = _temp1[i * 4 + 0]
            _temp2[i * 4 + 1] = _temp1[i * 4 + 1]
            _temp2[i * 4 + 2] = _temp1[i * 4 + 2]
            _temp2[i * 4 + 3] = _temp1[i * 4 + 3]

        _w[i * 4 + 0] = _w[(i - n_k) * 4 + 0] ^ _temp2[i * 4 + 0]
        _w[i * 4 + 1] = _w[(i - n_k) * 4 + 1] ^ _temp2[i * 4 + 1]
        _w[i * 4 + 2] = _w[(i - n_k) * 4 + 2] ^ _temp2[i * 4 + 2]
        _w[i * 4 + 3] = _w[(i - n_k) * 4 + 3] ^ _temp2[i * 4 + 3]
        i = i + 1

    debug_results = {
        "temp1": _temp1,
        "rotword": _rotword,
        "subword": _subword,
        "temp2": _temp2,
    }

    _return = (
        _w,
        debug_results,
    )

    return _return


def key_expansion_eic_bytes(
    key: bytes,
    r_con: bytes,
    n_k: int,
    n_r: int,
    n_b: int,
) -> tuple[bytes, dict]:
    """Function to expand AES key for use in the Equivalent Inverse Cipher."""
    (
        _w,
        _debug_results,
    ) = key_expansion_bytes(
        key,
        r_con,
        n_k,
        n_r,
        n_b,
    )

    _dw = _w

    for r in range(1, n_r):
        _dw[4 * r * n_b : 4 * (r + 1) * n_b] = inv_mix_columns_bytes(_dw[4 * r * n_b : 4 * (r + 1) * n_b], n_b)

    _debug_results["w"] = _w

    _return = (
        _dw,
        _debug_results,
    )

    return _return
